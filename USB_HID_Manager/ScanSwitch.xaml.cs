﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows;
using System.Linq;

namespace USB_HID_Manager
{
    /// <summary>
    /// Interaction logic for ScanSwitch.xaml
    /// </summary>
    public partial class ScanSwitch: IDisposable
    {
        private readonly SwitchManager _switchManager;
        private readonly UsbManager _usbManager;
        private string _switchName;

        public string SwitchName
        {
            get { return _switchName; }
        }

        private readonly List<SwitchPosition> _switchPositions = new List<SwitchPosition>(); 

        public ScanSwitch(SwitchManager switchManager, UsbManager usbManager, string switchName = null)
        {
            InitializeComponent();
            if (ConfigurationManager.AppSettings["Debug"].ToLower().Equals("true"))
            {
                Simulate.Visibility = Visibility.Visible;
            }

            if (!string.IsNullOrEmpty(switchName))
            {
                switchManager.DeleteSwitch(switchName);
            }
            _switchName = switchName;
            _switchManager = switchManager;
            _usbManager = usbManager;
            _switchManager.Scanning = true;
            _usbManager.UsbReceived += UsbManagerOnUsbReceived;
        }

        private void UsbManagerOnUsbReceived(object sender, UsbReceivedEventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(_switchName))
            {
                int i = 2;
                _switchName = "Switch 1";
                while (_switchManager.SwitchNameTaken(_switchName))
                {
                    _switchName = "Switch " + i++;
                }
            }
            List<SwitchPosition> newSwitchPositions = _switchManager.Scan(eventArgs.State, _switchName);
            if (newSwitchPositions == null)
            {
                return;
            }
            foreach (var pos in newSwitchPositions)
            {
                if (!_switchPositions.Any(p => p.Name.Equals(pos.Name)))
                {
                    _switchPositions.Add(pos);
                }
            }
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.BeginInvoke(new Action(() =>
                    {
                        NoOfSwitchPositions.Content = Convert.ToString(_switchPositions.Count);
                    }));
            }
            else
            {
                NoOfSwitchPositions.Content = Convert.ToString(_switchPositions.Count);
            }
        }

        private void ButtonOk_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public void Dispose()
        {
            _usbManager.UsbReceived -= UsbManagerOnUsbReceived;
            _switchManager.Scanning = false;
        }

        private void Simulate_OnClick(object sender, RoutedEventArgs e)
        {
            var bytes = new byte[6];
            bytes[2] = 0x01;
            _usbManager.Simulate(bytes);
            _usbManager.Simulate(bytes);
            bytes[2] = 0x00;
            _usbManager.Simulate(bytes);
        }
    }
}
