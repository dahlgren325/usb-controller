﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using USB_HID_Manager.Annotations;
using USB_HID_Manager.Properties;

namespace USB_HID_Manager
{
    public class LocalizedStrings: INotifyPropertyChanged
    {
        private static readonly Resources MyResources = new Resources();
        public Resources Resources { get { return MyResources; } }
        public event PropertyChangedEventHandler PropertyChanged;

        private static readonly LocalizedStrings TheInstance = new LocalizedStrings();

        public static LocalizedStrings Instance {
            get { return TheInstance; }
        }

        public void UpdatedCulture()
        {
            OnPropertyChanged("Resources");
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
