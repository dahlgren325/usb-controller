﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;

namespace USB_HID_Manager
{
    internal class AsyncResultNoResult : IAsyncResult
    {
        private ManualResetEvent _asyncWaitHandle;
        private readonly object _asyncState;
        private readonly AsyncCallback _asyncCallback;
        private Exception _exception;
        // Fields set at construction which do change after 
        // operation completes
        private const Int32 StatePending = 0;
        private const Int32 StateCompletedSynchronously = 1;
        private const Int32 StateCompletedAsynchronously = 2;
        private Int32 _completedState = StatePending;

        public AsyncResultNoResult(AsyncCallback asyncCallback, object state)
        {
            _asyncCallback = asyncCallback;
            _asyncState = state;
        }

        public bool IsCompleted
        {
            get { return Thread.VolatileRead(ref _completedState) != StatePending; }
        }

        public object AsyncState
        {
            get { return _asyncState; }
        }

        public bool CompletedSynchronously
        {
            get { return Thread.VolatileRead(ref _completedState) == StateCompletedSynchronously; }
        }

        public WaitHandle AsyncWaitHandle
        {
            get
            {
                if (_asyncWaitHandle == null)
                {
                    bool isCompleted = IsCompleted;
                    var mre = new ManualResetEvent(isCompleted);
                    if (Interlocked.CompareExchange(ref _asyncWaitHandle, mre, null) != null)
                    {
                        mre.Close();
                    }
                    else
                    {
                        if (!isCompleted && IsCompleted)
                        {
                            _asyncWaitHandle.Set();
                        }
                    }
                }
                return _asyncWaitHandle;
            }
        }

        public void SetAsCompleted(Exception exception, bool completedSynchronously)
        {
            _exception = exception;
            Int32 prevState = Interlocked.Exchange(ref _completedState, completedSynchronously ? StateCompletedSynchronously : StateCompletedAsynchronously);
            if (prevState != StatePending)
                throw new InvalidOperationException("You can set a result only once");
            // If the event exists, set it
            if (_asyncWaitHandle != null) _asyncWaitHandle.Set();
            // If a callback method was set, call it
            if (_asyncCallback != null) _asyncCallback(this);
        }

        public void EndInvoke()
        {
            if (!IsCompleted)
            {
                AsyncWaitHandle.WaitOne();
                AsyncWaitHandle.Close();
                _asyncWaitHandle = null;
            }
            if (_exception != null)
            {
                throw _exception;
            }
        }
    }

    internal class AsyncResult<TResult> : AsyncResultNoResult
    {
        private TResult _result;

        public AsyncResult(AsyncCallback asyncCallback, object state)
            : base(asyncCallback, state)
        {
        }

        public void SetAsCompleted(TResult result, bool completedSynchronously)
        {
            _result = result;
            SetAsCompleted(null, completedSynchronously);
        }

        public new TResult EndInvoke()
        {
            base.EndInvoke();
            return _result;
        }

    }

    internal class UsbAsyncResult<TResult> : AsyncResult<TResult>
    {
        private SafePinnedObject _outBuffer;

        public UsbAsyncResult(AsyncCallback asyncCallback, SafePinnedObject outBuffer, object state)
            : base(asyncCallback, state)
        {
            _outBuffer = outBuffer;
        }

        public unsafe NativeOverlapped* GetNativeOverlapped()
        {
            var o = new Overlapped(0, 0, IntPtr.Zero, this);
            return o.Pack(CompletionCallback, new[] { _outBuffer.Target });
        }

        private unsafe void CompletionCallback(uint errorcode, uint numbytes, NativeOverlapped* poverlap)
        {
            Overlapped.Free(poverlap);
            try
            {
                if (errorcode != 0)
                {
                    SetAsCompleted(new Win32Exception((int)errorcode), false);
                }
                else
                {
                    var result = (TResult)_outBuffer.Target;
// ReSharper disable CompareNonConstrainedGenericWithNull
                    if (result != null && result.GetType().IsArray)
// ReSharper restore CompareNonConstrainedGenericWithNull
                    {
                        Type elementType = result.GetType().GetElementType();
                        long numElements = numbytes / Marshal.SizeOf(elementType);
                        var origArray = (Array)(object)result;
                        if (numElements < origArray.Length)
                        {
                            Array newArray = Array.CreateInstance(elementType, numElements);
                            Array.Copy(origArray, newArray, numElements);
                            result = (TResult)(object)newArray;
                        }
                    }
                    SetAsCompleted(result, false);
                }
            }
            finally
            {
                _outBuffer.Dispose();
                _outBuffer = null;
            }
        }
    }
}