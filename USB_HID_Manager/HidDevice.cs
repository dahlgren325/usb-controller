﻿using System;
using System.ComponentModel;
using Microsoft.Win32.SafeHandles;

namespace USB_HID_Manager
{
    public class HidDevice: IDisposable
    {
        private static SafeFileHandle _handle;
        public string Path { get; set; }

        internal HidDevice(string path, SafeFileHandle handle)
        {
            Path = path;
            _handle = handle;
        }

        public byte[] GetReport()
        {
            const int size = 4;
            var bytes = new byte[size];
            bytes[0] = 0x01;
            unsafe
            {
                fixed (void* p = &bytes[0])
                {
                    bool hidDGetInputReport = PInvokeHelper.HidD_GetInputReport(_handle.DangerousGetHandle(), p, size);
                    int lastError = PInvokeHelper.GetLastError();
                    if (!hidDGetInputReport)
                    {
                        throw new Win32Exception(lastError);
                    }
                }
            }
            return bytes;
        }

        public unsafe IAsyncResult BeginRead(byte[] outBuffer, AsyncCallback callback, object state)
        {
            var pinnedBuffer = new SafePinnedObject(outBuffer);
            var asyncResult = new UsbAsyncResult<byte[]>(callback, pinnedBuffer, state);
            bool ok = PInvokeHelper.ReadFile(_handle.DangerousGetHandle(), outBuffer, (uint) outBuffer.Length, IntPtr.Zero, asyncResult.GetNativeOverlapped());
            int lastError = PInvokeHelper.GetLastError();
            if (ok)
            {
                return asyncResult;
            }
            if (lastError == 997)
            {
                return asyncResult;
            }
            if (lastError != 0)
            {
                throw new Win32Exception(lastError);
            }
            return asyncResult;
        }

        public byte[] EndRead(IAsyncResult asyncResult)
        {
            return ((UsbAsyncResult<byte[]>)asyncResult).EndInvoke();
        }

        public bool IsOpen
        {
            get
            {
                SafeFileHandle safeFileHandle = _handle;
                if (safeFileHandle == null)
                {
                    return false;
                }
                if (safeFileHandle.IsClosed)
                {
                    return false;
                }
                if (safeFileHandle.IsInvalid)
                {
                    return false;
                }
                return true;
            }
        }

        public void Close()
        {
            if (_handle == null)
            {
                return;
            }
            _handle.Close();
            _handle = null;
        }

        public void Dispose()
        {
            if (_handle == null)
            {
                return;
            }
            _handle.Close();
            _handle = null;
        }
    }
}
