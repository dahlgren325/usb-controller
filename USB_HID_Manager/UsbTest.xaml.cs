﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Windows;

namespace USB_HID_Manager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class UsbTest
    {
        private HidDevice _hidDevice;
        private readonly UsbManager _usbManager = new UsbManager();

        public UsbTest()
        {
            InitializeComponent();
        }

        public void OutputText(string text)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() =>
                    {
                        TextBox.AppendText(text + Environment.NewLine);
                        TextBox.ScrollToEnd();
                    });
            }
            else
            {
                TextBox.AppendText(text + Environment.NewLine);
                TextBox.ScrollToEnd();
            }
        }

        private void TestButtonOpen_OnClick(object sender, RoutedEventArgs e)
        {
            if (_hidDevice != null)
            {
                TextBox.Text = "Device already open";
                return;
            }
            // ReSharper disable PossibleNullReferenceException
            var productId = (int) new Int32Converter().ConvertFromString(ConfigurationManager.AppSettings["UsbProductId"]);
            var vendorId = (int)new Int32Converter().ConvertFromString(ConfigurationManager.AppSettings["UsbVendorId"]);
            // ReSharper restore PossibleNullReferenceException
            List<string> devices = HidHelper.EnumerateConnectedDevices(new DeviceDefinition { ProductId = (short)productId, VendorId = (short)vendorId });
            TextBox.Text = string.Join("\n", devices);
            if (devices.Count == 0)
            {
                TextBox.Text = "No device found";
                return;
            }
            _hidDevice = HidHelper.OpenDevice(devices[0]);
        }

        private void TestButtonRead_OnClick(object sender, RoutedEventArgs e)
        {
            if (_hidDevice == null)
            {
                TextBox.Text = "No device open";
                return;
            }
            var outBuffer = new byte[1024];
            _hidDevice.BeginRead(outBuffer, MyCallback, _hidDevice);
        }

        private void MyCallback(IAsyncResult ar)
        {
            var hidDevice = (HidDevice) ar.AsyncState;
            byte[] endRead = hidDevice.EndRead(ar);
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() =>
                    {
                        TextBox.Text = endRead.Aggregate("", (x, y) => x + y.ToString("X2"));
                    });
            }
        }

        private void TestButtonClose_OnClick(object sender, RoutedEventArgs e)
        {
            if (_hidDevice == null)
            {
                TextBox.Text = "No device open";
                return;
            }
            _hidDevice.Dispose();
            _hidDevice = null;
        }

        private void TestButtonStart_OnClick(object sender, RoutedEventArgs e)
        {
            _usbManager.UsbReceived += UsbManagerOnUsbReceived;
            _usbManager.Start();
        }

        private void UsbManagerOnUsbReceived(object sender, UsbReceivedEventArgs eventArgs)
        {
            
        }

        private void TestButtonStop_OnClick(object sender, RoutedEventArgs e)
        {
            _usbManager.Stop();
            _usbManager.UsbReceived -= UsbManagerOnUsbReceived;
        }
    }
}
