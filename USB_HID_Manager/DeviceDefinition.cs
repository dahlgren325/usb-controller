﻿using System.Collections.Generic;

namespace USB_HID_Manager
{
    public class DeviceDefinition
    {
        public short VendorId { get; set; }
        public short ProductId { get; set; }

        private sealed class VendorIdProductIdEqualityComparer : IEqualityComparer<DeviceDefinition>
        {
            public bool Equals(DeviceDefinition x, DeviceDefinition y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.VendorId == y.VendorId && x.ProductId == y.ProductId;
            }

            public int GetHashCode(DeviceDefinition obj)
            {
                unchecked
                {
                    return (obj.VendorId*397) ^ obj.ProductId;
                }
            }
        }

        private static readonly IEqualityComparer<DeviceDefinition> VendorIdProductIdComparerInstance = new VendorIdProductIdEqualityComparer();

        public static IEqualityComparer<DeviceDefinition> VendorIdProductIdComparer
        {
            get { return VendorIdProductIdComparerInstance; }
        }
    }
}
