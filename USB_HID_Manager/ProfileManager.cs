﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace USB_HID_Manager
{
    public class ProfileManager
    {
        public string LoadedProfile { get; set; }
        private readonly SwitchManager _switchManager;

        public ProfileManager(SwitchManager switchManager)
        {
            _switchManager = switchManager;
            Name = "New Profile";
        }

        public string Name { get; set; }

        public void Load(string file)
        {
            using (var fileStream = new FileStream(file, FileMode.Open))
            {
                var streamReader = new StreamReader(fileStream, Encoding.UTF8);
                streamReader.ReadLine(); // Header
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    if (line.TrimStart().StartsWith(";") || line.TrimStart().StartsWith("#") || line.TrimStart().StartsWith("//"))
                    {
                        continue;
                    }
                    if (string.IsNullOrEmpty(line.Trim()))
                    {
                        continue;
                    }
                    string[] strings = CsvSplit(line, ',');
                    if (strings.Length < 4)
                    {
                        throw new Exception("Each line needs to have at least 4 columns");
                    }
                    string switchName = strings[0].Trim();
                    string positionName = strings[1].Trim();
                    string mask = strings[2].Trim();
                    string state = strings[3].Trim();
                    if (!mask.StartsWith("0x") || !state.StartsWith("0x"))
                    {
                        throw new Exception("Mask and state should start with 0x");
                    }
                    mask = mask.Substring(2);
                    state = state.Substring(2);
                    byte[] maskArray = Enumerable.Range(0, mask.Length).Where(x => x%2 == 0).Select(x => Convert.ToByte(mask.Substring(x, 2), 16)).Reverse().ToArray();
                    byte[] stateArray = Enumerable.Range(0, state.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(state.Substring(x, 2), 16)).Reverse().ToArray();
                    HashSet<int> maskSignals = GetSignals(maskArray);
                    HashSet<int> stateSignals = GetSignals(stateArray);
                    var switchPosition = new SwitchPosition
                        {
                            Name = positionName, 
                            States = maskSignals.ToDictionary(maskSignal => maskSignal, stateSignals.Contains),
                            KeySetting = new KeySetting()
                        };
                    Switch sw = _switchManager.GetSwitch(switchName);
                    if (sw == null)
                    {
                        sw = new Switch(switchName, new List<SwitchPosition>
                            {
                                switchPosition
                            });
                        foreach (var b in maskSignals)
                        {
                            _switchManager.SignalSwitchMap.Add(b, sw);
                        }
                    }
                    else
                    {
                        sw.SwitchPositions.Add(switchPosition);
                    }
                    if (strings.Length < 6)
                    {
                        continue;
                    }
                    string type = strings[4];
                    KeySettingType keySettingType;
                    if (!Enum.TryParse(type, out keySettingType))
                    {
                        throw new Exception("Could not parse key setting type");
                    }
                    switchPosition.KeySetting.KeySettingType = keySettingType;
                    for (int i = 5; i < strings.Length; i += 2)
                    {
                        string vkeyCodeName = strings[i].Trim();
                        int vKeyCode = Array.IndexOf(KeyCode.VKeyCodeLabels, vkeyCodeName);
                        if (vKeyCode == -1)
                        {
                            if (!vkeyCodeName.StartsWith("0x"))
                            {
                                throw new Exception("Virtual key is not recognized and does not start with 0x");
                            }
                            vKeyCode = Convert.ToByte(vkeyCodeName.Substring(2), 16);
                        }
                        int scanCode = 0;
                        if (strings.Length > i + 1)
                        {
                            string scanCodeName = strings[i + 1].Trim();
                            if (!scanCodeName.StartsWith("0x"))
                            {
                                throw new Exception("Scan code is not recognized and does not start with 0x");
                            }
                            scanCode = (Convert.ToInt32(scanCodeName.Substring(2, 2), 16) << 16) + Convert.ToInt32(scanCodeName.Substring(4, 2), 16);
                        }
                        switchPosition.KeySetting.KeyCodes.Add(new KeyCode
                            {
                                Delay = 0,
                                VKeyCode = (uint) vKeyCode,
                                ScanCode = (uint) scanCode
                            });


                    }
                }
                streamReader.Close();
            }
            LoadedProfile = file;
            Name = Path.GetFileNameWithoutExtension(file);
        }

        public void Save()
        {
            Save(LoadedProfile);
        }

        public void Save(string file)
        {
            IEnumerable<Switch> allSwitches = _switchManager.GetAllSwitches();
            var sb = new StringBuilder();
            sb.AppendLine(
                "Switch, Position, Mask, State, Type, Virtual Key Code 1, Scan Code 1, Virtual Key Code 2, Scan Code 2, Virtual Key Code 3, Scan Code 3");
            foreach (var sw in allSwitches)
            {
                foreach (var sp in sw.SwitchPositions)
                {
                    if (sp.KeySetting == null)
                    {
                        sb.AppendLine(EscapeCsv(sw.Name, ',') + ", " +
                                      EscapeCsv(sp.Name, ',') + ", " +
                                      GetSwitchMask(sw) + ", " +
                                      GetSwitchState(sp));
                    }
                    else
                    {
                        sb.AppendLine(EscapeCsv(sw.Name, ',') + ", " + 
                            EscapeCsv(sp.Name, ',') + ", " +
                            GetSwitchMask(sw) + ", " +
                            GetSwitchState(sp) + ", " +
                            sp.KeySetting.KeySettingType.ToString() + ", " +
                            sp.KeySetting.KeyCodes.Aggregate("", (x, y) => x + (string.IsNullOrEmpty(x) ? "": ", ") + GetKeyCodeRepresentation(y)));
                    }
                }
            }
            using (var fileStream = new FileStream(file, FileMode.Create))
            {
                var streamWriter = new StreamWriter(fileStream, Encoding.UTF8);
                streamWriter.Write(sb.ToString());
                streamWriter.Close();
            }
            LoadedProfile = file;
            Name = Path.GetFileNameWithoutExtension(file);
        }

        private HashSet<int> GetSignals(IEnumerable<byte> bytes)
        {
            var result = new HashSet<int>();
            int pos = 1;
            foreach (byte t in bytes)
            {
                byte v = 0x01;
                for (int j = 0; j < 8; j++)
                {
                    bool bit = (t & v) != 0;
                    if (bit)
                    {
                        result.Add(pos);
                    }
                    v <<= 1;
                    pos++;
                }
            }
            return result;
        } 

        private string GetSwitchMask(Switch sw)
        {
            var signals = sw.Signals();
            int max = signals.Max();
            int noOfBytes = (max / 8) + 1;
            var mask = new int[noOfBytes];
            foreach (var signal in signals)
            {
                int b = (signal / 8);
                int bit = signal % 8 - 1;
                mask[b] += (1 << bit);
            }
            string r = mask.Reverse().Aggregate("", (current, i) => current + string.Format("{0:X2}", i));
            return "0x" + r;
        }

        private string GetSwitchState(SwitchPosition sp)
        {
            var signals = sp.States;
            int max = signals.Keys.Max();
            int noOfBytes = (max / 8) + 1;
            var mask = new int[noOfBytes];
            foreach (var signal in signals)
            {
                int b = (signal.Key / 8);
                int bit = signal.Key % 8 - 1;
                if (signal.Value)
                {
                    mask[b] += (1 << bit);
                }
            }
            string r = mask.Reverse().Aggregate("", (current, i) => current + string.Format("{0:X2}", i));
            return "0x" + r;
        }

        private string GetKeyCodeRepresentation(KeyCode keyCode)
        {
            string vKeyCodeLabel = KeyCode.VKeyCodeLabels[keyCode.VKeyCode];
            string r = string.IsNullOrEmpty(vKeyCodeLabel) ? string.Format("0x{0:X2}", keyCode.VKeyCode) : vKeyCodeLabel;
            r += ", " + string.Format("0x{0:X4}", keyCode.ScanCode);
            return r;
        }

        public static string EscapeCsv(String v, char separator)
        {
            if (v.IndexOf("\"", StringComparison.Ordinal) != -1)
            {
                v = v.Replace("\"", "\"\"");
                v = "\"" + v + "\"";
            }
            else if (v.IndexOf(separator) != -1)
            {
                v = "\"" + v + "\"";
            }
            else if (v.StartsWith(" ") || v.EndsWith(" "))
            {
                v = "\"" + v + "\"";
            }
            return v;
        }

        public static String[] CsvSplit(String v, char separator)
        {
            var result = new List<String>();
            bool quoted = false;
            String s = "";
            for (int i = 0; i < v.Length; i++)
            {
                if (v[i] == '"')
                {
                    if (quoted)
                    {
                        if (v.Length > i + 1 && v[i + 1] == '"')
                        {
                            s += '"';
                            i++;
                            continue;
                        }
                        if (v.Length <= i + 1 || v[i + 1] != '"')
                        {
                            quoted = false;
                            continue;
                        }
                    }
                    quoted = true;
                    continue;
                }
                if (v[i] == separator)
                {
                    if (quoted)
                    {
                        s += separator;
                        continue;
                    }
                    result.Add(s);
                    s = "";
                    continue;
                }
                s += v[i];
            }
            result.Add(s);

            return result.ToArray();
        }
    }
}