﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using USB_HID_Manager.Annotations;

namespace USB_HID_Manager
{
    public class SwitchPosition: INotifyPropertyChanged
    {
        public Dictionary<int, bool> States = new Dictionary<int, bool>();
        private string _name;
        private KeySetting _keySetting;
        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public KeySetting KeySetting
        {
            get { return _keySetting; }
            set
            {
                _keySetting = value;
                OnPropertyChanged();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
