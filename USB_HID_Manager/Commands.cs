﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;
using USB_HID_Manager.Annotations;

namespace USB_HID_Manager
{
    public static class Commands
    {
        public static RoutedUICommand Quit = new RoutedUICommand("Quit", "Quit", typeof(Commands),
                                                          new InputGestureCollection(new List<KeyGesture>
                                                              {
                                                                  new KeyGesture(Key.Q, ModifierKeys.Control)
                                                              }));
        public static RoutedUICommand Edit = new ExtendedRoutedUICommand("Edit", "Edit", typeof(Commands), Key.E);
        public static RoutedUICommand Save = new ExtendedRoutedUICommand("Save", "Save", typeof(Commands), Key.S);
        public static RoutedUICommand Ok = new ExtendedRoutedUICommand("Ok", "Ok", typeof(Commands), Key.Q);
        public static RoutedUICommand AddSwitch = new ExtendedRoutedUICommand("Add Switch", "AddSwitch", typeof(Commands), Key.A);
        public static RoutedUICommand RenameSwitch = new ExtendedRoutedUICommand("Rename Switch", "RenameSwitch", typeof(Commands), Key.T);
        public static RoutedUICommand Rescan = new ExtendedRoutedUICommand("Rescan", "Rescan", typeof(Commands), Key.S);
        public static RoutedUICommand Delete = new ExtendedRoutedUICommand("Delete", "Delete", typeof(Commands), Key.D);
        public static RoutedUICommand RenamePosition = new ExtendedRoutedUICommand("Rename", "RenamePosition", typeof(Commands), Key.N);
        public static RoutedUICommand PressAndRelease = new ExtendedRoutedUICommand("Press and Release", "PressAndRelease", typeof(Commands), Key.R);
        public static RoutedUICommand PressAndHold = new ExtendedRoutedUICommand("Press and Hold", "PressAndHold", typeof(Commands), Key.H);
        public static RoutedUICommand Macro = new ExtendedRoutedUICommand("Macro", "Macro", typeof(Commands), Key.M);
        public static RoutedUICommand Multiple = new ExtendedRoutedUICommand("Multiple", "Multiple", typeof(Commands));
        public static RoutedUICommand Clear = new ExtendedRoutedUICommand("Clear", "Clear", typeof(Commands));
        public static RoutedUICommand Cancel = new ExtendedRoutedUICommand("Cancel", "Cancel", typeof(Commands));
        public static RoutedUICommand OpenProfile = new ExtendedRoutedUICommand("OpenProfile", "OpenProfile", typeof(Commands));
    }

    public class ExtendedRoutedUICommand : RoutedUICommand
    {
        private readonly Key? _oneKeyBinding;

        public Key? OneKeyBinding { get { return _oneKeyBinding; } }

        public ExtendedRoutedUICommand([NotNull] string text, [NotNull] string name, [NotNull] Type ownerType)
            : base(text, name, ownerType)
        {
        }

        public ExtendedRoutedUICommand([NotNull] string text, [NotNull] string name, [NotNull] Type ownerType, Key oneKeyBinding)
            : base(text, name, ownerType)
        {
            _oneKeyBinding = oneKeyBinding;
        }

        public ExtendedRoutedUICommand([NotNull] string text, [NotNull] string name, [NotNull] Type ownerType, InputGestureCollection inputGestures)
            : base(text, name, ownerType, inputGestures)
        {
        }
    }

    public class CommandToTextValueConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var command = value as ExtendedRoutedUICommand;
            if (command != null)
            {
                if (command.OneKeyBinding != null)
                {
                    return command.Text + " [" + command.OneKeyBinding.Value.ToString() + "]";
                }
                return command.Text;
            }
            return value == null ? null : value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
