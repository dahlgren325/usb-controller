﻿using System;
using System.Windows;

namespace USB_HID_Manager
{
    /// <summary>
    /// Interaction logic for EditName.xaml
    /// </summary>
    public partial class EditName
    {
        public EditName(string title, string question, string defaultAnswer = "")
        {
            InitializeComponent();
            Title = title;
            Label1.Content = question;
            TextBox1.Text = defaultAnswer;
        }

        private void ButtonOk_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public string Answer { get { return TextBox1.Text; } }

        private void EditName_OnContentRendered(object sender, EventArgs e)
        {
            TextBox1.SelectAll();
            TextBox1.Focus();
        }
    }
}
