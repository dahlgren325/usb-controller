﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Win32.SafeHandles;

namespace USB_HID_Manager
{
    public class HidHelper
    {
        public static List<String> EnumerateConnectedDevices(DeviceDefinition deviceDefinition)
        {
            var result = new List<String>();
            var devicePaths = new List<String>();
            Guid guid;
            PInvokeHelper.HidD_GetHidGuid(out guid);
            IntPtr hDevInfo = PInvokeHelper.SetupDiGetClassDevs(ref guid, null, IntPtr.Zero, PInvokeHelper.DIGCF_PRESENT | PInvokeHelper.DIGCF_DEVICEINTERFACE);
            bool success = true;
            uint i = 0;
            while (success)
            {
                var iData = new SP_DEVICE_INTERFACE_DATA();
                iData.cbSize = Marshal.SizeOf(iData);
                success = PInvokeHelper.SetupDiEnumDeviceInterfaces(hDevInfo, IntPtr.Zero, ref guid, i++, ref iData);
                if (!success)
                {
                    continue;
                }
                var spDevinfoData = new SP_DEVINFO_DATA();
                spDevinfoData.cbSize = (uint)Marshal.SizeOf(spDevinfoData);
                var detailData = new SP_DEVICE_INTERFACE_DETAIL_DATA();
                if (IntPtr.Size == 8) // for 64 bit operating systems
                    detailData.cbSize = 8;
                else
                    detailData.cbSize = 4 + Marshal.SystemDefaultCharSize;
                uint bufferSize;
#pragma warning disable 168
                var safePinnedObject1 = new SafePinnedObject(spDevinfoData);
                var safePinnedObject2 = new SafePinnedObject(iData);
#pragma warning restore 168
                PInvokeHelper.SetupDiGetDeviceInterfaceDetail(hDevInfo, ref iData, IntPtr.Zero, 0, out bufferSize, ref spDevinfoData);
                IntPtr detailDataBuffer = Marshal.AllocHGlobal((int)bufferSize + 1000);
                Marshal.StructureToPtr(detailData, detailDataBuffer, false);
                PInvokeHelper.SetupDiGetDeviceInterfaceDetail(hDevInfo, ref iData, detailDataBuffer, bufferSize, out bufferSize, ref spDevinfoData);
                detailData = (SP_DEVICE_INTERFACE_DETAIL_DATA)Marshal.PtrToStructure(detailDataBuffer, detailData.GetType());
                Marshal.FreeHGlobal(detailDataBuffer);
                devicePaths.Add(detailData.DevicePath);
            }
            PInvokeHelper.SetupDiDestroyDeviceInfoList(hDevInfo);
            foreach (var path in devicePaths)
            {
                SafeHandle handle = PInvokeHelper.CreateFile(path, PInvokeHelper.GENERIC_READ | PInvokeHelper.GENERIC_WRITE, PInvokeHelper.FILE_SHARE_READ | PInvokeHelper.FILE_SHARE_WRITE, IntPtr.Zero, 3, PInvokeHelper.FILE_FLAG_OVERLAPPED, IntPtr.Zero);
                if (handle.IsInvalid)
                {
                    continue;
                }
                var attributes = new ATTRIBUTES();
                attributes.Size = Marshal.SizeOf(attributes);
                bool hidDGetAttributes = PInvokeHelper.HidD_GetAttributes(handle.DangerousGetHandle(), ref attributes);
                if (hidDGetAttributes && attributes.ProductID.Equals(deviceDefinition.ProductId) && attributes.VendorID.Equals(deviceDefinition.VendorId))
                {
                    result.Add(path);
                }
                handle.Close();
                //PInvokeHelper.CloseHandle(handle.DangerousGetHandle());
            }
            return result;
        }

        public static HidDevice OpenDevice(string devicePath)
        {
            SafeFileHandle handle = PInvokeHelper.CreateFile(devicePath, PInvokeHelper.GENERIC_READ | PInvokeHelper.GENERIC_WRITE, PInvokeHelper.FILE_SHARE_READ | PInvokeHelper.FILE_SHARE_WRITE, IntPtr.Zero, 3, PInvokeHelper.FILE_FLAG_OVERLAPPED, IntPtr.Zero);
            if (handle.IsInvalid)
            {
                throw new Win32Exception();
            }
            int lastError = PInvokeHelper.GetLastError();
            if (lastError != 0)
            {
                throw new Win32Exception(lastError);
            }
            ThreadPool.BindHandle(handle);
            return new HidDevice(devicePath, handle);
        }
    }
}
