﻿using System.Collections.Generic;
using System.Windows.Input;

namespace USB_HID_Manager
{
    public class KeyCode
    {
        public uint VKeyCode { get; set; }

        public uint ScanCode { get; set; }

        public uint Delay { get; set; }

        public static string[] VKeyCodeLabels = new[]
            {
                "LBUTTON", "RBUTTON", "CANCEL", "MBUTTON", "XBUTTON1", "XBUTTON2", "BACK", "", "BACK", "TAB", // 0-9 0x00-0x09
                "", "", "CLEAR", "RETURN", "", "", "SHIFT", "CONTROL", "MENU", "PAUSE", // 10-19 0x0A-0x13
                "CAPITAL", "KANA", "HANGUL", "JUNJA", "FINAL", "HANJA", "KANJI", "ESCAPE", "CONVERT", "NONCONVERT", // 20-29 0x14-0x1D
                "ACCEPT", "MODECHANGE", "SPACE", "PRIOR", "NEXT", "END", "HOME", "LEFT", "UP", "RIGHT", // 30-39 0x1E-0x27
                "DOWN", "SELECT", "PRINT", "EXECUTE", "SNAPSHOT", "INSERT", "DELETE", "HELP", "0", "1", // 40-49 0x28-0x31
                "2", "3", "4", "5", "6", "7", "8", "9", "", "", // 50-59 0x32-0x3B
                "", "", "", "", "", "A", "B", "C", "D", "E", // 60-69 0x3E-0x45
                "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", // 70-79 0x46-0x4F
                "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", // 80-89 0x50-0x59
                "Z", "LWIN", "RWIN", "APPS", "", "SLEEP", "Num0", "Num1", "Num2", "Num3", // 90-99 0x5A-0x63
                "Num4", "Num5", "Num6", "Num7", "Num8", "Num9", "MULTIPLY", "ADD", "SEPARATOR", "SUBTRACT", // 100-109 0x64-0x6D
                "DECIMAL", "DIVIDE", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", // 110-119 0x6E-0x77
                "F9", "F10", "F11", "F12", "F13", "F14", "F15", "F16", "F17", "F18", // 120-129 0x78-0x81
                "F19", "F20", "F21", "F22", "F23", "F24", "", "", "", "", // 130-139 0x82-0x8B
                "", "", "", "", "NUMLOCK", "SCROLL", "", "", "", "", // 140-149 0x8C-0x95
                "", "", "", "", "", "", "", "", "", "", // 150-159 0x96-0x9F
                "LSHIFT", "RSHIFT", "LCONTROL", "RCONTROL", "LMENU", "RMENU", "BROWSER_BACK", "BROWSER_FORWARD", "BROWSER_REFRESH", "BROWSER_STOP", // 160-169 0xA0-0xA9
                "BROWSER_SEARCH", "BROWSER_FAVORITES", "BROWSER_HOME", "VOLUME_MUTE", "VOLUME_DOWN", "VOLUME_UP", "MEDIA_NEXT_TRACK", "MEDIA_PREV_TRACK", "MEDIA_STOP", "MEDIA_PLAY_PAUSE", // 170-179 0xAA-0xB3
                "LAUNCH_MAIL", "LAUNCH_MEDIA_SELECT", "LAUNCH_APP1", "LAUNCH_APP2", "", "", "OEM_1", "OEM_PLUS", "OEM_COMMA", "OEM_MINUS", // 180-189 0xB4-0xBD
                "OEM_PERIOD", "OEM_2", "OEM_3", "", "", "", "", "", "", "", // 190-199 0xBE-0xC7
                "", "", "", "", "", "", "", "", "", "", // 200-209 0xC8-0xD1
                "", "", "", "", "", "", "", "", "", "OEM_4", // 210-219 0xD2-0xDB
                "OEM_5", "OEM_6", "OEM_7", "OEM_8", "", "", "OEM_102", "", "", "PROCESSKEY", // 220-229 0xDC-0xE5
                "", "PACKET", "", "", "", "", "", "", "", "", // 230-239 0xE6-0xEA
                "", "", "", "", "", "", "ATTN", "CRSEL", "EXSEL", "EREOF", // 240-249 0xEB-0xF9
                "PLAY", "ZOOM", "NONAME", "PA1", "OEM_CLEAR", "" // 250-255 0xFA-0xFF
            };

        protected bool Equals(KeyCode other)
        {
            return VKeyCode == other.VKeyCode && ScanCode == other.ScanCode;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((KeyCode) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int) VKeyCode*397) ^ (int) ScanCode;
            }
        }

        public static bool operator ==(KeyCode left, KeyCode right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(KeyCode left, KeyCode right)
        {
            return !Equals(left, right);
        }
    }

    public enum KeySettingType
    {
        PressAndRelease,
        PressAndHold,
        Macro
    }

    public class KeySetting
    {
        private KeySettingType _keySettingType = KeySettingType.PressAndRelease;
        private List<KeyCode> _keyCodes = new List<KeyCode>();

        public KeySettingType KeySettingType
        {
            get { return _keySettingType; }
            set { _keySettingType = value; }
        }

        public List<KeyCode> KeyCodes
        {
            get { return _keyCodes; }
            set { _keyCodes = value; }
        }
    }

    public class KeyMappings
    {
         List<KeyMapping> _keyMappings = new List<KeyMapping>
             {
                 new KeyMapping(Key.A, VirtualKeyShort.KEY_A),
                 new KeyMapping(Key.B, VirtualKeyShort.KEY_B),
                 new KeyMapping(Key.C, VirtualKeyShort.KEY_C),
                 new KeyMapping(Key.D, VirtualKeyShort.KEY_D),
                 new KeyMapping(Key.E, VirtualKeyShort.KEY_E),
                 new KeyMapping(Key.F, VirtualKeyShort.KEY_F),
                 new KeyMapping(Key.G, VirtualKeyShort.KEY_G),
                 new KeyMapping(Key.H, VirtualKeyShort.KEY_H),
                 new KeyMapping(Key.I, VirtualKeyShort.KEY_I),
                 new KeyMapping(Key.J, VirtualKeyShort.KEY_J),
                 new KeyMapping(Key.K, VirtualKeyShort.KEY_K),
                 new KeyMapping(Key.L, VirtualKeyShort.KEY_L),
                 new KeyMapping(Key.M, VirtualKeyShort.KEY_M),
                 new KeyMapping(Key.N, VirtualKeyShort.KEY_N),
                 new KeyMapping(Key.O, VirtualKeyShort.KEY_O),
                 new KeyMapping(Key.P, VirtualKeyShort.KEY_P),
                 new KeyMapping(Key.Q, VirtualKeyShort.KEY_Q),
                 new KeyMapping(Key.R, VirtualKeyShort.KEY_R),
                 new KeyMapping(Key.S, VirtualKeyShort.KEY_S),
                 new KeyMapping(Key.T, VirtualKeyShort.KEY_T),
                 new KeyMapping(Key.U, VirtualKeyShort.KEY_U),
                 new KeyMapping(Key.V, VirtualKeyShort.KEY_V),
                 new KeyMapping(Key.W, VirtualKeyShort.KEY_W),
                 new KeyMapping(Key.X, VirtualKeyShort.KEY_X),
                 new KeyMapping(Key.Y, VirtualKeyShort.KEY_Y),
                 new KeyMapping(Key.Z, VirtualKeyShort.KEY_Z),
                 new KeyMapping(Key.F1, VirtualKeyShort.F1),
                 new KeyMapping(Key.F2, VirtualKeyShort.F2),
                 new KeyMapping(Key.F3, VirtualKeyShort.F3),
                 new KeyMapping(Key.F4, VirtualKeyShort.F4),
                 new KeyMapping(Key.F5, VirtualKeyShort.F5),
                 new KeyMapping(Key.F6, VirtualKeyShort.F6),
                 new KeyMapping(Key.F7, VirtualKeyShort.F7),
                 new KeyMapping(Key.F8, VirtualKeyShort.F8),
                 new KeyMapping(Key.F9, VirtualKeyShort.F9),
                 new KeyMapping(Key.F10, VirtualKeyShort.F10),
                 new KeyMapping(Key.F11, VirtualKeyShort.F11),
                 new KeyMapping(Key.F12, VirtualKeyShort.F12),
                 new KeyMapping(Key.F13, VirtualKeyShort.F13),
                 new KeyMapping(Key.F14, VirtualKeyShort.F14),
                 new KeyMapping(Key.F15, VirtualKeyShort.F15),
                 new KeyMapping(Key.F16, VirtualKeyShort.F16),
                 new KeyMapping(Key.F17, VirtualKeyShort.F17),
                 new KeyMapping(Key.F18, VirtualKeyShort.F18),
                 new KeyMapping(Key.F19, VirtualKeyShort.F19),
                 new KeyMapping(Key.F20, VirtualKeyShort.F20),
                 new KeyMapping(Key.F21, VirtualKeyShort.F21),
                 new KeyMapping(Key.F22, VirtualKeyShort.F22),
                 new KeyMapping(Key.F23, VirtualKeyShort.F23),
                 new KeyMapping(Key.F24, VirtualKeyShort.F24),
                 new KeyMapping(Key.Add, VirtualKeyShort.ADD),
                 new KeyMapping(Key.Back, VirtualKeyShort.BACK),
                 //new KeyMapping(Key.CapsLock, VirtualKeyShort.),
                 new KeyMapping(Key.Capital, VirtualKeyShort.CAPITAL),
                 new KeyMapping(Key.Clear, VirtualKeyShort.CLEAR),
                 new KeyMapping(Key.Decimal, VirtualKeyShort.DECIMAL),
                 new KeyMapping(Key.Delete, VirtualKeyShort.DELETE),
                 new KeyMapping(Key.Divide, VirtualKeyShort.DIVIDE),
                 new KeyMapping(Key.Down, VirtualKeyShort.DOWN),
                 new KeyMapping(Key.End, VirtualKeyShort.END),
                 new KeyMapping(Key.Enter, VirtualKeyShort.RETURN),
                 new KeyMapping(Key.Escape, VirtualKeyShort.ESCAPE),
                 new KeyMapping(Key.Home, VirtualKeyShort.HOME),
                 new KeyMapping(Key.Insert, VirtualKeyShort.INSERT),
                 new KeyMapping(Key.LWin, VirtualKeyShort.LWIN),
                 new KeyMapping(Key.Left, VirtualKeyShort.LEFT),
                 //new KeyMapping(Key.LeftAlt, VirtualKeyShort.),
                 new KeyMapping(Key.LeftCtrl, VirtualKeyShort.LCONTROL),
                 new KeyMapping(Key.LeftShift, VirtualKeyShort.LSHIFT),
                 new KeyMapping(Key.Multiply, VirtualKeyShort.MULTIPLY),
                 new KeyMapping(Key.NumLock, VirtualKeyShort.NUMLOCK),
                 new KeyMapping(Key.NumPad0, VirtualKeyShort.NUMPAD0),
                 new KeyMapping(Key.NumPad1, VirtualKeyShort.NUMPAD1),
                 new KeyMapping(Key.NumPad2, VirtualKeyShort.NUMPAD2),
                 new KeyMapping(Key.NumPad3, VirtualKeyShort.NUMPAD3),
                 new KeyMapping(Key.NumPad4, VirtualKeyShort.NUMPAD4),
                 new KeyMapping(Key.NumPad5, VirtualKeyShort.NUMPAD5),
                 new KeyMapping(Key.NumPad6, VirtualKeyShort.NUMPAD6),
                 new KeyMapping(Key.NumPad7, VirtualKeyShort.NUMPAD7),
                 new KeyMapping(Key.NumPad8, VirtualKeyShort.NUMPAD8),
                 new KeyMapping(Key.NumPad9, VirtualKeyShort.NUMPAD9),
                 //new KeyMapping(Key.PageDown, VirtualKeyShort.),
                 //new KeyMapping(Key.PageUp, VirtualKeyShort.),
                 new KeyMapping(Key.Pause, VirtualKeyShort.PAUSE),
                 new KeyMapping(Key.Print, VirtualKeyShort.PRINT),
                 new KeyMapping(Key.RWin, VirtualKeyShort.RWIN),
                 new KeyMapping(Key.Return, VirtualKeyShort.RETURN),
                 new KeyMapping(Key.Right, VirtualKeyShort.RIGHT),
                 //new KeyMapping(Key.RightAlt, VirtualKeyShort.),
                 new KeyMapping(Key.RightCtrl, VirtualKeyShort.RCONTROL),
                 new KeyMapping(Key.RightShift, VirtualKeyShort.RSHIFT),
                 new KeyMapping(Key.Separator, VirtualKeyShort.SEPARATOR),
                 new KeyMapping(Key.Space, VirtualKeyShort.SPACE),
                 new KeyMapping(Key.Subtract, VirtualKeyShort.SUBTRACT),
                 new KeyMapping(Key.Tab, VirtualKeyShort.TAB),
                 new KeyMapping(Key.Up, VirtualKeyShort.UP),
                 new KeyMapping(Key.D0, VirtualKeyShort.KEY_0),
                 new KeyMapping(Key.D1, VirtualKeyShort.KEY_1),
                 new KeyMapping(Key.D2, VirtualKeyShort.KEY_2),
                 new KeyMapping(Key.D3, VirtualKeyShort.KEY_3),
                 new KeyMapping(Key.D4, VirtualKeyShort.KEY_4),
                 new KeyMapping(Key.D5, VirtualKeyShort.KEY_5),
                 new KeyMapping(Key.D6, VirtualKeyShort.KEY_6),
                 new KeyMapping(Key.D7, VirtualKeyShort.KEY_7),
                 new KeyMapping(Key.D8, VirtualKeyShort.KEY_8),
                 new KeyMapping(Key.D9, VirtualKeyShort.KEY_9),
             }; 
    }

    public class KeyMapping
    {
        private Key _key;
        private VirtualKeyShort _scanCode;
        private short _overridingScanCode;

        public KeyMapping(Key key, VirtualKeyShort scanCode)
        {
            _key = key;
            _scanCode = scanCode;
        }
    }
}