﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace USB_HID_Manager
{
    public class SwitchManager
    {
        private byte[] _lastState;
        private readonly Dictionary<int, Switch> _signalSwitchMap = new Dictionary<int, Switch>();

        public bool Scanning { get; set; }

        public int LastStateLength
        {
            get { return _lastState == null ? 0 : _lastState.Length; }
        }

        public void InitializeLastState(byte[] b)
        {
            if (b != null && b.Length != 0)
            {
                _lastState = new byte[b.Length];
                Array.Copy(b, _lastState, b.Length);
            }
        }

        public Dictionary<int, Switch> SignalSwitchMap
        {
            get { return _signalSwitchMap; }
        }

        public IEnumerable<Switch> GetAllSwitches()
        {
            var dictionary = new Dictionary<string, Switch>();
            foreach (var sw in _signalSwitchMap.Values)
            {
                if (!dictionary.ContainsKey(sw.Name))
                {
                    dictionary.Add(sw.Name, sw);
                }
            }
            return dictionary.Values;
        }

        public void Clear()
        {
            _signalSwitchMap.Clear();
        }

        public bool SwitchNameTaken(string switchName)
        {
            if (_signalSwitchMap.Count == 0)
            {
                return false;
            }
            return _signalSwitchMap.Any(sw => sw.Value.Name.Equals(switchName));
        }

        public void DeleteSwitch(string name)
        {
            IEnumerable<KeyValuePair<int, Switch>> keyValuePairs = _signalSwitchMap.Where(e => e.Value.Name.Equals(name)).ToList();
            foreach (var keyValuePair in keyValuePairs)
            {
                _signalSwitchMap.Remove(keyValuePair.Key);
            }
        }

        public Switch GetSwitch(string name)
        {
            return _signalSwitchMap.Values.FirstOrDefault(sw => sw.Name.Equals(name));
        }

        // Returns newly detected switch position or null
        public List<SwitchPosition> Scan(byte[] state, string switchName)
        {
            if (_lastState == null)
            {
                return null;
            }
            if (_lastState.SequenceEqual(state))
            {
                return null;
            }
            if (state.Length != _lastState.Length)
            {
                throw new ArgumentException("State array doesn't have a consistent length");
            }
            var newSwitchPositions = new List<SwitchPosition>();
            Dictionary<int, bool> changedSignals = GetChangedSignals(state);
            foreach (var changedSignal in changedSignals)
            {
                if (_signalSwitchMap.ContainsKey(changedSignal.Key))
                {
                    // Known signal has changed
                    Switch sw = _signalSwitchMap[changedSignal.Key];
                    if (sw.Name.Equals(switchName)) // Only scan signals for current switch
                    {
                        Dictionary<int, bool> signalsForSwitch = GetSignalsForSwitch(sw, state);
                        SwitchPosition switchPosition = sw.GetSwitchPosition(signalsForSwitch);
                        if (switchPosition == null)
                        {
                            // New position detected
                            newSwitchPositions.Add(sw.RegisterSwitchPosition(GetFreeSwitchPositionName(sw), signalsForSwitch));
                        }
                    }
                    //else
                    //{
                    //    // Signal associated with another switch has changed, ignore
                    //}
                }
                else
                {
                    // New signal. Assume it should be associated with the current switch.
                    Switch sw = _signalSwitchMap.Values.FirstOrDefault(s => s.Name.Equals(switchName));
                    if (sw == null)
                    {
                        // Completely new switch
                        // Set up 2 switch positions for previous and current signal value
                        var switchPositions = new List<SwitchPosition> {
                                new SwitchPosition
                                {
                                    Name = "Position 1",
                                    States = new Dictionary<int, bool> {{changedSignal.Key, !changedSignal.Value}}
                                },
                                new SwitchPosition
                                {
                                    Name = "Position 2",
                                    States = new Dictionary<int, bool> {{changedSignal.Key, changedSignal.Value}}
                                },
                            };
                        newSwitchPositions.AddRange(switchPositions);
                        // New switch
                        _signalSwitchMap.Add(changedSignal.Key, new Switch(switchName, switchPositions));
                    }
                    else
                    {
                        // New signal and new position in existing switch
                        sw.AddSignal(changedSignal.Key, !changedSignal.Value);
                        Dictionary<int, bool> signalsForSwitch = GetSignalsForSwitch(sw, state);
                        newSwitchPositions.Add(sw.RegisterSwitchPosition(GetFreeSwitchPositionName(sw), signalsForSwitch));
                        _signalSwitchMap.Add(changedSignal.Key, sw);
                    }
                }
            }
            Array.Copy(state, _lastState, state.Length);
            return newSwitchPositions;
        }

        public string GetFreeSwitchPositionName(Switch sw)
        {
            int i = 1;
            do
            {
                string name = "Position " + i++;
                if (!sw.SwitchPositionNameIsTaken(name))
                    return name;
            } while (true);
        }

        public Dictionary<string, SwitchPosition> GetAllPositions(byte[] state)
        {
            var switchPositions = new Dictionary<string, SwitchPosition>();
            Dictionary<int, bool> allSignals = GetAllSignals(state);
            foreach (var signal in allSignals)
            {
                if (_signalSwitchMap.ContainsKey(signal.Key))
                {
                    Switch sw = _signalSwitchMap[signal.Key];
                    if (!switchPositions.ContainsKey(sw.Name))
                    {
                        Dictionary<int, bool> signalsForSwitch = GetSignalsForSwitch(sw, state);
                        SwitchPosition switchPosition = sw.GetSwitchPosition(signalsForSwitch);
                        switchPositions.Add(sw.Name, switchPosition);
                    }
                }
            }
            return switchPositions;
        }

        // Returns updated switch positions
        public Dictionary<string, SwitchPosition> StateMessage(byte[] state)
        {
            if (Scanning)
            {
                return null;
            }
            if (_lastState == null)
            {
                return null;
            }
            if (_lastState.SequenceEqual(state))
            {
                return null;
            }
            if (state.Length != _lastState.Length)
            {
                throw new ArgumentException("State array doesn't have a consistent length");
            }
            var switchPositions = new Dictionary<string, SwitchPosition>();
            Dictionary<int, bool> changedSignals = GetChangedSignals(state);
            foreach (var changedSignal in changedSignals)
            {
                if (_signalSwitchMap.ContainsKey(changedSignal.Key))
                {
                    Switch sw = _signalSwitchMap[changedSignal.Key];
                    if (!switchPositions.ContainsKey(sw.Name))
                    {
                        Dictionary<int, bool> signalsForSwitch = GetSignalsForSwitch(sw, state);
                        SwitchPosition switchPosition = sw.GetSwitchPosition(signalsForSwitch);
                        if (switchPosition != null)
                        {
                            switchPositions.Add(sw.Name, switchPosition);
                        }
                    }
                }
            }
            Array.Copy(state, _lastState, state.Length);
            return switchPositions;
        }

        private Dictionary<int, bool> GetSignalsForSwitch(Switch sw, byte[] state)
        {
            HashSet<int> signals = sw.Signals();
            var signalMap = new Dictionary<int, bool>();
            foreach (var signal in signals)
            {
                int b = (signal / 8);
                int bit = signal%8 - 1;
                var v = (byte)(1 << bit);
                bool isSet = (state[b] & v) != 0;
                signalMap[signal] = isSet;
            }
            return signalMap;
        }

        private Dictionary<int, bool> GetChangedSignals(byte[] state)
        {
            var changedSignals = new Dictionary<int, bool>();
            int pos = 1;
            for (int i = 0; i < state.Length; i++)
            {
                if (_lastState[i] != state[i])
                {
                    byte v = 0x01;
                    for (int j = 0; j < 8; j++)
                    {
                        bool newBit = (state[i] & v) != 0;
                        bool oldBit = (_lastState[i] & v) != 0;
                        if (newBit != oldBit)
                        {
                            changedSignals.Add(pos, newBit);
                        }
                        v <<= 1;
                        pos++;
                    }
                }
                else
                {
                    pos += 8;
                }
            }
            return changedSignals;
        }

        private Dictionary<int, bool> GetAllSignals(IEnumerable<byte> state)
        {
            var allSignals = new Dictionary<int, bool>();
            int pos = 1;
            foreach (byte b in state)
            {
                byte v = 0x01;
                for (int j = 0; j < 7; j++)
                {
                    bool bit = (b & v) != 0;
                    allSignals.Add(pos, bit);
                    v <<= 1;
                    pos++;
                }
            }
            return allSignals;
        }
    }
}
