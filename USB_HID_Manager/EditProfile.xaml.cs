﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Linq;
using USB_HID_Manager.Annotations;

namespace USB_HID_Manager
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditProfile: IDisposable, INotifyPropertyChanged
    {
        #region Backing Fields

        private readonly SwitchManager _switchManager;
        private readonly UsbManager _usbManager;
        private Switch _activeSwitch;
        private SwitchPosition _activeSwitchPosition;

        #endregion

        #region Properties

        public SwitchPosition ActiveSwitchPosition
        {
            get { return _activeSwitchPosition; }
            set
            {
                _activeSwitchPosition = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<SwitchInfo> Switches { get; set; }

        public SwitchInfo ActiveSwitchInfo
        {
            get
            {
                return _activeSwitch == null ? null : Switches.FirstOrDefault(si => si.Name.Equals(_activeSwitch.Name));
            }
            set
            {
                _activeSwitch = value == null ? null : _switchManager.GetSwitch(value.Name);
                OnPropertyChanged();
            }
        }

        #endregion

        public EditProfile(SwitchManager switchManager, UsbManager usbManager)
        {
            Switches = new ObservableCollection<SwitchInfo>();
            InitializeComponent();
            if (ConfigurationManager.AppSettings["Debug"].ToLower().Equals("true"))
            {
                Simulate.Visibility = Visibility.Visible;
            }
            KeyDown += OnKeyDown;
            usbManager.UsbReceived += UsbManagerOnUsbReceived;
            _switchManager = switchManager;
            _usbManager = usbManager;
            foreach (Switch sw in _switchManager.GetAllSwitches().OrderBy(sw => sw.Name))
            {
                Switches.Add(new SwitchInfo{ Name = sw.Name, Postitions = sw.SwitchPositions.Count});
            }
        }

        #region Events

        private void UsbManagerOnUsbReceived(object sender, UsbReceivedEventArgs eventArgs)
        {
            var switchPositions = new Dictionary<string, SwitchPosition>();
            int lastStateLength = _switchManager.LastStateLength; // TODO get report
            if (lastStateLength == 0)
            {
                lastStateLength = eventArgs.State.Length;
            }
            for (int i = 0; i < eventArgs.State.Length; i += lastStateLength)
            {
                var bytes = new byte[lastStateLength];
                Array.Copy(eventArgs.State, i, bytes, 0, lastStateLength);
                Dictionary<string, SwitchPosition> r = _switchManager.StateMessage(bytes);
                if (r != null)
                foreach (var v in r)
                {
                    switchPositions[v.Key] = v.Value;
                }
            }
            if (switchPositions.Count != 0)
            {
                KeyValuePair<string, SwitchPosition> keyValuePair = switchPositions.First();
                string switchName = keyValuePair.Key;
                SwitchPosition switchPosition = keyValuePair.Value;
                ActiveSwitchPosition = switchPosition;
                ActiveSwitchInfo = Switches.FirstOrDefault(si => si.Name.Equals(switchName));
                if (!Dispatcher.CheckAccess())
                {
                    Dispatcher.Invoke(CommandManager.InvalidateRequerySuggested
                );
                }
                else
                {
                    CommandManager.InvalidateRequerySuggested();
                }
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            foreach (CommandBinding commandBinding in CommandBindings)
            {
                ICommand command = commandBinding.Command;
                var uiCommand = command as ExtendedRoutedUICommand;
                if (uiCommand != null && uiCommand.OneKeyBinding != null)
                {
                    if (keyEventArgs.Key == uiCommand.OneKeyBinding)
                    {
                        uiCommand.Execute(null, null);
                    }
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Command Implementations

        private void AddSwitch(object sender, ExecutedRoutedEventArgs e)
        {
            using (var scanSwitch = new ScanSwitch(_switchManager, _usbManager))
            {
                if (scanSwitch.ShowDialog() == false)
                {
                    _switchManager.DeleteSwitch(scanSwitch.SwitchName);
                    return;
                }
                Switch sw = _switchManager.GetSwitch(scanSwitch.SwitchName);
                if (sw == null)
                {
                    return;
                }
                var editName = new EditName(Properties.Resources.SwitchName, Properties.Resources.EnterNameOfNewSwitch, sw.Name);
                if (editName.ShowDialog() == false)
                {
                    _switchManager.DeleteSwitch(sw.Name);
                    return;
                }
                string switchName = editName.Answer;
                if (string.IsNullOrEmpty(switchName))
                {
                    _switchManager.DeleteSwitch(sw.Name);
                    return;
                }
                var dictionary = new Dictionary<string, string>();
                foreach (var switchPosition in sw.SwitchPositions.OrderBy(s => s.Name))
                {
                    editName = new EditName(Properties.Resources.PositionName, Properties.Resources.EnterNameOfSwitchPosition, switchPosition.Name);
                    if (editName.ShowDialog() == false)
                    {
                        _switchManager.DeleteSwitch(sw.Name);
                        return;
                    }
                    if (string.IsNullOrEmpty(editName.Answer))
                    {
                        _switchManager.DeleteSwitch(sw.Name);
                        return;
                    }
                    dictionary.Add(switchPosition.Name, editName.Answer);
                }
                sw.Name = switchName;
                foreach (var switchPosition in sw.SwitchPositions)
                {
                    if (dictionary.ContainsKey(switchPosition.Name))
                    {
                        switchPosition.Name = dictionary[switchPosition.Name];
                    }
                }
                Switches.AddSorted(new SwitchInfo{Name = sw.Name, Postitions = sw.SwitchPositions.Count});
            }
        }

        private void RenameSwitch(object sender, ExecutedRoutedEventArgs e)
        {
            if (ActiveSwitchInfo == null)
            {
                return;
            }
            var editName = new EditName(Properties.Resources.ChangeSwitchName, Properties.Resources.SwitchName, ActiveSwitchInfo.Name);
            if (editName.ShowDialog() == false)
            {
                return;
            }
            string switchName = editName.Answer;
            if (string.IsNullOrEmpty(switchName))
            {
                return;
            }
            SwitchInfo si = ActiveSwitchInfo;
            _activeSwitch.Name = switchName;
            Switches.Remove(si);
            si.Name = switchName;
            Switches.AddSorted(si);
        }

        private void Rescan(object sender, ExecutedRoutedEventArgs e)
        {
            SwitchInfo activeSwitchInfo = ActiveSwitchInfo;
            if (activeSwitchInfo == null)
            {
                return;
            }
            using (var scanSwitch = new ScanSwitch(_switchManager, _usbManager, activeSwitchInfo.Name))
            {
                if (scanSwitch.ShowDialog() == false)
                {
                    return;
                }
                Switch sw = _switchManager.GetSwitch(scanSwitch.SwitchName);
                if (sw == null)
                {
                    return;
                }
                var editName = new EditName(Properties.Resources.SwitchName, Properties.Resources.EnterNameOfNewSwitch, sw.Name);
                if (editName.ShowDialog() == false)
                {
                    return;
                }
                string switchName = editName.Answer;
                if (string.IsNullOrEmpty(switchName))
                {
                    return;
                }
                var dictionary = new Dictionary<string, string>();
                foreach (var switchPosition in sw.SwitchPositions.OrderBy(s => s.Name))
                {
                    editName = new EditName(Properties.Resources.PositionName, Properties.Resources.EnterNameOfSwitchPosition, switchPosition.Name);
                    if (editName.ShowDialog() == false)
                    {
                        return;
                    }
                    if (string.IsNullOrEmpty(editName.Answer))
                    {
                        return;
                    }
                    dictionary.Add(switchPosition.Name, editName.Answer);
                }

                sw.Name = switchName;
                foreach (var switchPosition in sw.SwitchPositions)
                {
                    if (dictionary.ContainsKey(switchPosition.Name))
                    {
                        switchPosition.Name = dictionary[switchPosition.Name];
                    }
                }

                Switches.Remove(activeSwitchInfo);
                activeSwitchInfo.Name = switchName;
                activeSwitchInfo.Postitions = sw.SwitchPositions.Count;
                Switches.AddSorted(activeSwitchInfo);
            }
        }

        private void Delete(object sender, ExecutedRoutedEventArgs e)
        {
            SwitchInfo activeSwitchInfo = ActiveSwitchInfo;
            if (activeSwitchInfo == null)
            {
                return;
            }
            _switchManager.DeleteSwitch(activeSwitchInfo.Name);
            Switches.Remove(activeSwitchInfo);
            ActiveSwitchInfo = null;
        }

        private void RenamePosition(object sender, ExecutedRoutedEventArgs e)
        {
            if (ActiveSwitchPosition == null)
            {
                return;
            }
            var editName = new EditName(Properties.Resources.ChangePositionName, Properties.Resources.PositionName, ActiveSwitchPosition.Name);
            if (editName.ShowDialog() == false)
            {
                return;
            }
            string posName = editName.Answer;
            if (string.IsNullOrEmpty(posName))
            {
                return;
            }
            ActiveSwitchPosition.Name = posName;
        }

        private void PressAndRelease(object sender, ExecutedRoutedEventArgs e)
        {
            var keySettingForm = new KeySettingForm(_activeSwitchPosition, KeySettingType.PressAndRelease);
            keySettingForm.ShowDialog();
            OnPropertyChanged("ActiveSwitchPosition");
        }

        private void PressAndHold(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void Macro(object sender, ExecutedRoutedEventArgs e)
        {
        }

        public void Ok(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void Simulate_OnClick(object sender, RoutedEventArgs e)
        {
            var bytes = new byte[6];
            bytes[2] = 0x01;
            _usbManager.Simulate(bytes);
            bytes[2] = 0x00;
            _usbManager.Simulate(bytes);
        }

        #endregion

        #region Command CanExecute Implementations

        private void CanExecuteTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CanExecuteRenameSwitch(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ListViewSwitches.SelectedItems.Count == 1;
        }

        private void CanExecuteRescan(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ListViewSwitches.SelectedItems.Count == 1;
        }

        private void CanExecuteDelete(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ListViewSwitches.SelectedItems.Count > 0;
        }

        private void CanExecuteRenamePosition(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ActiveSwitchPosition != null;
        }

        #endregion

        #region Model Classes

        public class SwitchInfo : INotifyPropertyChanged, IComparable<SwitchInfo>
        {
            private string _name;
            private int _postitions;

            public string Name
            {
                get { return _name; }
                set
                {
                    if (value == _name) return;
                    _name = value;
                    OnPropertyChanged();
                }
            }

            public int Postitions
            {
                get { return _postitions; }
                set
                {
                    if (value == _postitions) return;
                    _postitions = value;
                    OnPropertyChanged();
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }

            public int CompareTo(SwitchInfo other)
            {
                if (_name == null)
                {
                    return 1;
                }
                if (other.Name == null)
                {
                    return -1;
                }
                return String.Compare(_name, other.Name, StringComparison.Ordinal);
            }
        }

        #endregion

        public void Dispose()
        {
            _usbManager.UsbReceived -= UsbManagerOnUsbReceived;
        }

    }

    #region Value Converters

    public class KeySettingTypeToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var keySettingTypeString = (string)parameter;
            KeySettingType keySettingType;
            Enum.TryParse(keySettingTypeString, out keySettingType);
            var switchPosition = (SwitchPosition)value;
            if (switchPosition == null)
            {
                return false;
            }
            if (switchPosition.KeySetting == null)
            {
                return false;
            }
            if (switchPosition.KeySetting.KeySettingType != keySettingType)
            {
                return false;
            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SwitchPositionToFunctionTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var switchPosition = (SwitchPosition)value;
            if (switchPosition == null)
            {
                return "";
            }
            if (switchPosition.KeySetting == null)
            {
                return "";
            }
            if (switchPosition.KeySetting.KeyCodes.Count == 0)
            {
                return "";
            }
            string txt;
            switch (switchPosition.KeySetting.KeySettingType)
            {
                case KeySettingType.PressAndRelease:
                    txt = "PR ";
                    break;
                case KeySettingType.PressAndHold:
                    txt = "PH ";
                    break;
                case KeySettingType.Macro:
                    txt = "M ";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            foreach (var keyCode in switchPosition.KeySetting.KeyCodes)
            {
                if (string.IsNullOrEmpty(txt))
                {
                    txt = KeyCode.VKeyCodeLabels[keyCode.VKeyCode];
                }
                else
                {
                    txt += " " + KeyCode.VKeyCodeLabels[keyCode.VKeyCode];
                }
            }
            return txt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    #endregion

    #region Helper Extensions

    public static class ListExtensions
    {
        public static void AddSorted<T>(this IList<T> list, T item, IComparer<T> comparer = null)
        {
            if (comparer == null)
                comparer = Comparer<T>.Default;

            int i = 0;
            while (i < list.Count && comparer.Compare(list[i], item) < 0)
                i++;

            list.Insert(i, item);
        }
    }

    #endregion

}
