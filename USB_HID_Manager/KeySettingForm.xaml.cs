﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;

namespace USB_HID_Manager
{
    /// <summary>
    /// Interaction logic for KeySetting.xaml
    /// </summary>
    public partial class KeySettingForm
    {
        public bool MultipleKeys { get; set; }

        private readonly SwitchPosition _switchPosition;
        private readonly KeySetting _keySetting;

        public KeySettingForm(SwitchPosition switchPosition, KeySettingType keySettingType)
        {
            _switchPosition = switchPosition;
            _keySetting = switchPosition.KeySetting ?? new KeySetting();
            _keySetting.KeySettingType = keySettingType;
            MultipleKeys = _keySetting.KeyCodes.Count > 1;
            InitializeComponent();
            UpdateKeyLabel();
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            var source = PresentationSource.FromVisual(this) as HwndSource;
            Debug.Assert(source != null, "source != null");
            source.AddHook(WndProc);
        }

        private void AddKeyCode(KeyCode keyCode)
        {
            if (_keySetting.KeyCodes.Count > 2)
            {
                return;
            }
            if (_keySetting.KeyCodes.Contains(keyCode))
            {
                return;
            }
            _keySetting.KeyCodes.Add(keyCode);
            UpdateKeyLabel();
        }

        private void UpdateKeyLabel()
        {
            string txt = "";
            foreach (var kc in _keySetting.KeyCodes)
            {
                if (string.IsNullOrEmpty(txt))
                {
                    txt += KeyCode.VKeyCodeLabels[kc.VKeyCode];
                }
                else
                {
                    txt += " " + KeyCode.VKeyCodeLabels[kc.VKeyCode];
                }
            }
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => { Buttons.Content = txt; });
            }
            else
            {
                Buttons.Content = txt;
            }
        }

        private void ClearKeyCodes()
        {
            _keySetting.KeyCodes.Clear();
            UpdateKeyLabel();
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam, ref bool handled)
        {
            if (msg == (decimal)PInvokeHelper.WM.KEYDOWN || msg == (decimal)PInvokeHelper.WM.SYSKEYDOWN)
            {
                int vKey = wparam.ToInt32();
                int scan = (lparam.ToInt32() & 0x00FF0000) >> 16;
                if (!MultipleKeys)
                {
                    _keySetting.KeyCodes.Clear();
                }
                AddKeyCode(new KeyCode {Delay = 0, ScanCode = (uint) scan, VKeyCode = (uint) vKey});
                if (!MultipleKeys)
                {
                    _switchPosition.KeySetting = _keySetting;
                    DialogResult = true;
                    Close();
                }
            }
            return IntPtr.Zero;
        }

        private void Ok(object sender, ExecutedRoutedEventArgs e)
        {
            _switchPosition.KeySetting = _keySetting;
            DialogResult = true;
            Close();
        }

        private void CanExecuteTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Multiple(object sender, ExecutedRoutedEventArgs e)
        {
            if (MultipleButton.IsChecked != null && !MultipleButton.IsChecked.Value)
            {
                _keySetting.KeyCodes = _keySetting.KeyCodes.Take(1).ToList();
                UpdateKeyLabel();
            }
        }

        private void Clear(object sender, ExecutedRoutedEventArgs e)
        {
            ClearKeyCodes();
        }

        private void CanExecuteClear(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _keySetting.KeyCodes.Count != 0;
        }

        private void CanExecuteOk(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
    }
}
