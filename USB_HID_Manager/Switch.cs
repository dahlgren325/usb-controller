﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace USB_HID_Manager
{
    public class Switch
    {
        public string Name;
        private readonly List<SwitchPosition> _switchPositions = new List<SwitchPosition>();

        public List<SwitchPosition> SwitchPositions
        {
            get { return _switchPositions; }
        }

        public Switch(string name, IEnumerable<SwitchPosition> switchPositions)
        {
            Name = name;
            _switchPositions.AddRange(switchPositions);

        }

        public bool SwitchPositionNameIsTaken(string switchPositionName)
        {
            return _switchPositions.Any(switchPosition => switchPosition.Name.Equals(switchPositionName));
        }

        public HashSet<int> Signals()
        {
            var signals = new HashSet<int>();
            foreach (var switchPosition in _switchPositions)
            {
                foreach (var key in switchPosition.States.Keys)
                {
                    signals.Add(key);
                }

            }
            return signals;
        }

        public void AddSignal(int signal, bool defaulValue)
        {
            foreach (var switchPosition in _switchPositions)
            {
                if (!switchPosition.States.ContainsKey(signal))
                {
                    switchPosition.States.Add(signal, defaulValue);
                }
            }
        }

        public SwitchPosition RegisterSwitchPosition(string name, Dictionary<int, bool> signals)
        {
            SwitchPosition existingSwitchPosition = GetSwitchPosition(signals);
            if (existingSwitchPosition != null)
            {
                throw new ArgumentException("Switch position already registered, name: " + existingSwitchPosition.Name);
            }
            HashSet<int> activeSignals = Signals();
            var newPosition = new SwitchPosition {Name = name};
            foreach (var signal in activeSignals)
            {
                if (!signals.ContainsKey(signal))
                {
                    throw new ArgumentException("Signal " + signal + " missing");
                }
                newPosition.States[signal] = signals[signal];
            }
            _switchPositions.Add(newPosition);
            return newPosition;
        }

        public SwitchPosition GetSwitchPosition(Dictionary<int, bool> signals)
        {
            foreach (var switchPosition in _switchPositions)
            {
                bool found = true;
                foreach (var state in switchPosition.States)
                {
                    if (!signals.ContainsKey(state.Key))
                    {
                        found = false;
                        break;
                    }
                    if (signals[state.Key] != state.Value)
                    {
                        found = false;
                        break;
                    }
                }
                if (found)
                {
                    return switchPosition;
                }
            }
            return null;
        }
    }
}
