﻿using System.Collections.Generic;
using System.Diagnostics;

namespace USB_HID_Manager
{
    public class SwitchManagerTest
    {
        public void TestScan()
        {
            var switchManager = new SwitchManager();
            var buttonStates = new byte[1];
            List<SwitchPosition> switchPositions = switchManager.Scan(buttonStates, "Switch 1");
            Debug.Assert(switchPositions.Count == 0);
            buttonStates[0] = 0x02;
            switchPositions = switchManager.Scan(buttonStates, "Switch 1");
            Debug.Assert(switchPositions.Count == 2);
            buttonStates[0] = 0x0;
            switchPositions = switchManager.Scan(buttonStates, "Switch 1");
            Debug.Assert(switchPositions.Count == 0);
            buttonStates[0] = 0x3;
            switchPositions = switchManager.Scan(buttonStates, "Switch 1");
            Debug.Assert(switchPositions.Count == 1);
            buttonStates[0] = 0x2;
            switchPositions = switchManager.Scan(buttonStates, "Switch 1");
            Debug.Assert(switchPositions.Count == 0);
            buttonStates[0] = 0x0;
            switchPositions = switchManager.Scan(buttonStates, "Switch 1");
            Debug.Assert(switchPositions.Count == 0);
            buttonStates[0] = 0x1;
            switchPositions = switchManager.Scan(buttonStates, "Switch 1");
            Debug.Assert(switchPositions.Count == 1);
            buttonStates[0] = 0x2;
            switchPositions = switchManager.Scan(buttonStates, "Switch 1");
            Debug.Assert(switchPositions.Count == 0);
            buttonStates[0] = 0x0;
            switchPositions = switchManager.Scan(buttonStates, "Switch 2");
            Debug.Assert(switchPositions.Count == 0);
            buttonStates[0] = 0x3;
            switchPositions = switchManager.Scan(buttonStates, "Switch 2");
            Debug.Assert(switchPositions.Count == 0);
            buttonStates[0] = 0x4;
            switchPositions = switchManager.Scan(buttonStates, "Switch 2");
            Debug.Assert(switchPositions.Count == 2);
            buttonStates[0] = 0x5;
            switchPositions = switchManager.Scan(buttonStates, "Switch 2");
            Debug.Assert(switchPositions.Count == 0);
            buttonStates[0] = 0x3;
            Dictionary<string, SwitchPosition> v = switchManager.StateMessage(buttonStates);
            Debug.Assert(v.Count == 2);
            buttonStates[0] = 0x5;
            v = switchManager.StateMessage(buttonStates);
            Debug.Assert(v.Count == 2);
            buttonStates[0] = 0x7;
            v = switchManager.StateMessage(buttonStates);
            Debug.Assert(v.Count == 1);
        }
    }
}
