﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Threading;
using System.Linq;

namespace USB_HID_Manager
{
    public interface IUsbReceiver
    {
        void UsbReceived(object sender, byte[] message);
    }

    public class UsbReceivedEventArgs: EventArgs
    {
        public UsbReceivedEventArgs(byte[] state)
        {
            State = state;
        }

        public byte[] State { get; set; }
    }

    public enum EventType { Connected, Disconnected };

    public class DeviceConnectedEventArgs : EventArgs
    {

        public DeviceConnectedEventArgs(string path, EventType eventType)
        {
            Path = path;
            EventType = eventType;
        }

        public string Path { get; set; }
        public EventType EventType { get; set; }
    }

    class HidDeviceAsyncHolder
    {
        public HidDevice HidDevice { get; set; }

        public IAsyncResult AsyncResult { get; set; }
    }

    public class UsbManager: IUsbReceiver, IDisposable
    {
        private readonly List<HidDeviceAsyncHolder> _hidDevices = new List<HidDeviceAsyncHolder>();
        private readonly object _mutex = new object();
        private bool _running;
        private bool _signalStop;

        private void EnumerateDevices()
        {
            // ReSharper disable PossibleNullReferenceException
            var productId = (int)new Int32Converter().ConvertFromString(ConfigurationManager.AppSettings["UsbProductId"]);
            var vendorId = (int)new Int32Converter().ConvertFromString(ConfigurationManager.AppSettings["UsbVendorId"]);
            // ReSharper restore PossibleNullReferenceException
            List<string> devices = HidHelper.EnumerateConnectedDevices(new DeviceDefinition { ProductId = (short)productId, VendorId = (short)vendorId });
            foreach (var device in devices)
            {
                if (!_hidDevices.Any(d => d.HidDevice.Path.Equals(device)))
                {
                    HidDevice hidDevice = HidHelper.OpenDevice(device);
                    IAsyncResult asyncResult = hidDevice.BeginRead(new byte[1024], Callback, hidDevice);
                    _hidDevices.Add(new HidDeviceAsyncHolder {HidDevice = hidDevice, AsyncResult = asyncResult});
                    OnDeviceConnected(new DeviceConnectedEventArgs(device, EventType.Connected));
                }
            }
            IEnumerable<HidDeviceAsyncHolder> toRemove = _hidDevices.Where(hd => !devices.Contains(hd.HidDevice.Path)).ToList();
            foreach (var hd in toRemove)
            {
                OnDeviceConnected(new DeviceConnectedEventArgs(hd.HidDevice.Path, EventType.Disconnected));
                hd.HidDevice.Close();
                _hidDevices.Remove(hd);
            }
        }

        public void Simulate(byte[] b)
        {
            OnUsbReceivedEventHandler(new UsbReceivedEventArgs(b));
        }

        private void Callback(IAsyncResult ar)
        {
            try
            {
                var hidDevice = (HidDevice)ar.AsyncState;
                byte[] bytes = hidDevice.EndRead(ar);
                OnUsbReceivedEventHandler(new UsbReceivedEventArgs(bytes));
                HidDeviceAsyncHolder dh = _hidDevices.FirstOrDefault(h => h.HidDevice.Path.Equals(hidDevice.Path));
                if (dh != null)
                {
                    if (_signalStop)
                    {
                        return;
                    }
                    if (!_running)
                    {
                        return;
                    }
                    if (!hidDevice.IsOpen)
                    {
                        return;
                    }
                    if (dh.AsyncResult.IsCompleted)
                    {
                        dh.AsyncResult = hidDevice.BeginRead(new byte[1024], Callback, hidDevice);
                    }
                }
            }
            catch (Win32Exception e)
            {
                if ((_signalStop || !_running) && e.NativeErrorCode == 995) // Application ending
                {
                    return;
                }
                if (e.NativeErrorCode == 1167) // Not connected
                {
                    return;
                }
                throw;
            }
        }

        private void MainLoop()
        {
            _running = true;
            _signalStop = false;
            Thread.MemoryBarrier();
            int count = 10;
            new Thread(() =>
            {
                while (true)
                {
                    Thread.MemoryBarrier();
                    Thread.Sleep(100);
                    if (_signalStop)
                    {
                        _running = false;
                        Thread.MemoryBarrier();
                        _signalStop = false;
                        Thread.MemoryBarrier();
                        break;
                    }
                    if (count-- <= 0)
                    {
                        EnumerateDevices();
                        count = 10;
                    }
                }
            }).Start();

        }

        public void Start()
        {
            lock (_mutex)
            {
                MainLoop();
            }
        }

        public void Stop()
        {
            lock (_mutex)
            {
                _signalStop = true;
                Thread.MemoryBarrier();
                foreach (var hidDevice in _hidDevices)
                {
                    hidDevice.HidDevice.Close();
                }
                _hidDevices.Clear();
            }
        }

        public byte[] GetReport(string device)
        {
            HidDeviceAsyncHolder h = _hidDevices.FirstOrDefault(hd => hd.HidDevice.Path.Equals(device));
            if (h == null)
            {
                return null;
            }
            byte[] report = h.HidDevice.GetReport();
            return report;
        }

        public Dictionary<string, byte[]> GetReport()
        {
            var result = new Dictionary<string, byte[]>();
            foreach (var d in _hidDevices)
            {
                byte[] report = d.HidDevice.GetReport();
                result.Add(d.HidDevice.Path, report);
            }
            return result;
        }

        void IUsbReceiver.UsbReceived(object sender, byte[] message)
        {
            if (!_signalStop && _running)
            {
                OnUsbReceivedEventHandler(new UsbReceivedEventArgs(message));
            }
        }


        public void Dispose()
        {
            Stop();
        }

        #region Events

        public delegate void DeviceConnectedEventHandler(object sender, DeviceConnectedEventArgs eventArgs);

        public event DeviceConnectedEventHandler DeviceConnected;

        protected virtual void OnDeviceConnected(DeviceConnectedEventArgs eventargs)
        {
            DeviceConnectedEventHandler handler = DeviceConnected;
            if (handler != null) handler(this, eventargs);
        }

        public delegate void UsbReceivedEventHandler(object sender, UsbReceivedEventArgs eventArgs);

        public event UsbReceivedEventHandler UsbReceived;

        protected void OnUsbReceivedEventHandler(UsbReceivedEventArgs eventArgs)
        {
            UsbReceivedEventHandler usbReceived = UsbReceived;
            if (usbReceived != null)
            {
                usbReceived(this, eventArgs);
            }
        }

        #endregion
    }
}
