﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace USB_HID_Manager
{
    class SafePinnedObject: SafeHandleZeroOrMinusOneIsInvalid
    {
        private GCHandle _gcHandle;

        public SafePinnedObject(object obj): base(true)
        {
            if (obj == null)
            {
                return;
            }
            _gcHandle = GCHandle.Alloc(obj, GCHandleType.Pinned);
            SetHandle(_gcHandle.AddrOfPinnedObject());
        }

        protected override bool ReleaseHandle()
        {
            SetHandle(IntPtr.Zero);
            _gcHandle.Free();
            return true;
        }

        public object Target
        {
            get { return IsInvalid ? null : _gcHandle.Target; }
        }

        public Int32 Size
        {
            get
            {
                object target = Target;
                if (target == null)
                {
                    return 0;
                }
                if (!target.GetType().IsArray)
                {
                    return Marshal.SizeOf(target);
                }
                var a = (Array) target;
                return a.Length*Marshal.SizeOf(a.GetType().GetElementType());
            }
        }
    }
}
