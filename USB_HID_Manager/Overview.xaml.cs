﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Win32;
using System.Linq;
using USB_HID_Manager.Annotations;
using USB_HID_Manager.Properties;

namespace USB_HID_Manager
{
    /// <summary>
    /// Interaction logic for Overview.xaml
    /// </summary>
    public sealed partial class Overview: IDisposable, INotifyPropertyChanged
    {
        private readonly ProfileManager _profileManager;
        private readonly SwitchManager _switchManager = new SwitchManager();
        private readonly UsbManager _usbManager = new UsbManager();
        private readonly ObservableCollection<string> _devices = new ObservableCollection<string>();
        private bool _isEditing;
        private readonly ConcurrentQueue<KeySetting> _keySettingQueue = new ConcurrentQueue<KeySetting>();
        private bool _endSignal;
        private readonly AutoResetEvent _queueFlag = new AutoResetEvent(false);
        private readonly ObservableCollection<LastProfiles> _lastProfiles = new ObservableCollection<LastProfiles>();
        private bool _hasKeyboardFocus = true;

        public ObservableCollection<LastProfiles> LastProfiles
        {
            get { return _lastProfiles; }
        }

        public ProfileManager ProfileManager
        {
            get { return _profileManager; }
        }

        public ObservableCollection<string> Devices
        {
            get { return _devices; }
        }

        public Overview()
        {
            InitializeComponent();
            if (ConfigurationManager.AppSettings["Debug"].ToLower().Equals("true"))
            {
                TestMenu.Visibility = Visibility.Visible;
            }
            KeyDown += OnKeyDown;
            _usbManager.DeviceConnected += UsbManagerOnDeviceConnected;
            _usbManager.UsbReceived += UsbManagerOnUsbReceived;
            _usbManager.Start();
            _profileManager = new ProfileManager(_switchManager);
            OnPropertyChanged("ProfileManager");
            StringCollection recentFiles = Settings.Default.RecentFiles;
            foreach (var recentFile in recentFiles)
            {
                _lastProfiles.Add(new LastProfiles { File = recentFile, Name = Path.GetFileNameWithoutExtension(recentFile) });
            }
            KeyLoop();
        }

        public void KeyLoop()
        {
            new Thread(RunQueueLoop).Start();
        }

        private void RunQueueLoop()
        {
            while (!_endSignal)
            {
                KeySetting keySetting;
                if (!_keySettingQueue.TryDequeue(out keySetting))
                {
                    keySetting = null;
                }
                while (keySetting != null)
                {
                    if (!_hasKeyboardFocus && keySetting.KeyCodes.Count != 0)
                    {
                        if (keySetting.KeySettingType == KeySettingType.PressAndRelease)
                        {
                            var inputs = new INPUT[keySetting.KeyCodes.Count];
                            for (int i = 0; i < keySetting.KeyCodes.Count; i++)
                            {
                                inputs[i] = new INPUT { type = 1, U = new InputUnion { ki = new KEYBDINPUT { wVk = (VirtualKeyShort)keySetting.KeyCodes[i].VKeyCode, wScan = (ScanCodeShort)keySetting.KeyCodes[i].ScanCode, dwFlags = 0, time = 0 } } };
                            }
                            PInvokeHelper.SendInput((uint)inputs.Length, inputs, Marshal.SizeOf(inputs[0]));
                            Thread.Sleep(100);
                            for (int i = keySetting.KeyCodes.Count - 1; i >= 0; i--)
                            {
                                inputs[i] = new INPUT { type = 1, U = new InputUnion { ki = new KEYBDINPUT { wVk = (VirtualKeyShort)keySetting.KeyCodes[i].VKeyCode, wScan = (ScanCodeShort)keySetting.KeyCodes[i].ScanCode, dwFlags = KEYEVENTF.KEYUP, time = 0 } } };
                            }
                            PInvokeHelper.SendInput((uint)inputs.Length, inputs, Marshal.SizeOf(inputs[0]));
                        }
                    }
                    if (!_keySettingQueue.TryDequeue(out keySetting))
                    {
                        keySetting = null;
                    }
                }
                _queueFlag.WaitOne();
                Thread.MemoryBarrier();
            }
        }

        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnLostKeyboardFocus(e);
            _hasKeyboardFocus = false;
            Thread.MemoryBarrier();
        }

        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnGotKeyboardFocus(e);
            _hasKeyboardFocus = true;
            Thread.MemoryBarrier();
        }

        private void QueueKeySettings(IEnumerable<KeySetting> keySettings)
        {
            foreach (var keySetting in keySettings)
            {
                _keySettingQueue.Enqueue(keySetting);
            }
            Thread.MemoryBarrier();
            _queueFlag.Set();
        }

        private void UsbManagerOnUsbReceived(object sender, UsbReceivedEventArgs eventArgs)
        {
            Thread.MemoryBarrier();
            if (_isEditing)
            {
                return;
            }
            var switchPositions = new Dictionary<string, SwitchPosition>();
            int lastStateLength = _switchManager.LastStateLength; // TODO get from report
            if (lastStateLength == 0)
            {
                lastStateLength = eventArgs.State.Length;
            }
            for (int i = 0; i < eventArgs.State.Length; i += lastStateLength)
            {
                var bytes = new byte[lastStateLength];
                Array.Copy(eventArgs.State, i, bytes, 0, lastStateLength);
                Dictionary<string, SwitchPosition> r = _switchManager.StateMessage(bytes);
                if (r != null)
                    foreach (var v in r)
                    {
                        switchPositions[v.Key] = v.Value;
                    }
            }

            if (switchPositions.Count != 0)
            {
                QueueKeySettings(switchPositions.Values.Select(sp => sp.KeySetting));
            }
        }

        private void UsbManagerOnDeviceConnected(object sender, DeviceConnectedEventArgs eventArgs)
        {
            if (eventArgs.EventType == EventType.Connected)
            {
                if (!_devices.Contains(eventArgs.Path))
                {
                    _devices.Add(eventArgs.Path);
                    byte[] report = _usbManager.GetReport(eventArgs.Path);
                    _switchManager.InitializeLastState(report);
                }
            }
            else
            {
                _devices.Remove(eventArgs.Path);
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            foreach (CommandBinding commandBinding in CommandBindings)
            {
                ICommand command = commandBinding.Command;
                var uiCommand = command as ExtendedRoutedUICommand;
                if (uiCommand != null && uiCommand.OneKeyBinding != null)
                {
                    if (keyEventArgs.Key == uiCommand.OneKeyBinding)
                    {
                        uiCommand.Execute(null, null);
                    }
                }
            }
        }

        private void CanExecuteTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Edit(object sender, ExecutedRoutedEventArgs e)
        {
            _isEditing = true;
            Thread.MemoryBarrier();
            try
            {
                using (
                    var editWindow = new EditProfile(_switchManager, _usbManager)
                    {
                        WindowStartupLocation = WindowStartupLocation.CenterScreen
                    })
                {
                    editWindow.ShowDialog();
                }
            }
            finally
            {
                _isEditing = false;
                Thread.MemoryBarrier();
            }
        }

        private void Save(object sender, ExecutedRoutedEventArgs e)
        {
            if (_profileManager.LoadedProfile == null)
            {
                var saveFileDialog = new SaveFileDialog
                {
                    Filter = "Profile (*.bpf)|*.bpf",
                    CheckPathExists = true,
                    AddExtension = true
                };
                if (saveFileDialog.ShowDialog(this).Value)
                {
                    string fileName = saveFileDialog.FileName;
                    _profileManager.Save(fileName);
                    UpdateLastProfiles(fileName);
                    OnPropertyChanged("ProfileManager");
                }
            }
            else
            {
                _profileManager.Save();
            }
        }

        private void Quit(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void Open(object sender, ExecutedRoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Profile (*.bpf)|*.bpf",
                CheckFileExists = true,
            };
            if (openFileDialog.ShowDialog(this).Value)
            {
                _switchManager.Clear();
                _profileManager.Load(openFileDialog.FileName);
                UpdateLastProfiles(openFileDialog.FileName);
            }
        }

        private void TestSwitchScan_OnClick(object sender, RoutedEventArgs e)
        {
            new SwitchManagerTest().TestScan();
        }

        private void UsbTest_OnClick(object sender, RoutedEventArgs e)
        {
            var usbTest = new UsbTest();
            usbTest.Show();
        }

        protected override void OnClosed(EventArgs e)
        {
            _endSignal = true;
            Thread.MemoryBarrier();
            _queueFlag.Set();
            _usbManager.DeviceConnected -= UsbManagerOnDeviceConnected;
            _usbManager.UsbReceived -= UsbManagerOnUsbReceived;
            _usbManager.Stop();
            base.OnClosed(e);
        }

        public void Dispose()
        {
        }

        private void New(object sender, ExecutedRoutedEventArgs e)
        {
            _switchManager.Clear();
            _profileManager.LoadedProfile = null;
            _profileManager.Name = "New Profile";
            OnPropertyChanged("ProfileManager");
        }

        private void SaveAs(object sender, ExecutedRoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Profile (*.bpf)|*.bpf",
                CheckPathExists = true,
                AddExtension = true
            };
            if (saveFileDialog.ShowDialog(this).Value)
            {
                string fileName = saveFileDialog.FileName;
                _profileManager.Save(fileName);
                UpdateLastProfiles(fileName);
                OnPropertyChanged("ProfileManager");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OpenProfile(object sender, ExecutedRoutedEventArgs e)
        {
            var lastProfile = e.Parameter as LastProfiles;
            if (lastProfile == null)
            {
                return;
            }
            _switchManager.Clear();
            if (!File.Exists(lastProfile.File))
            {
                MessageBox.Show("Profile does not exist.");
                RemoveLastProfile(lastProfile.File);
                return;
            }
            _profileManager.Load(lastProfile.File);
            UpdateLastProfiles(lastProfile.File);
        }

        private void RemoveLastProfile(string filename)
        {
            var lastProfiles = new LastProfiles { File = filename, Name = Path.GetFileNameWithoutExtension(filename) };
            if (_lastProfiles.Contains(lastProfiles))
            {
                _lastProfiles.Remove(lastProfiles);
                SaveLastProfiles();
                OnPropertyChanged("ProfileManager");
            }
        }

        private void UpdateLastProfiles(string filename)
        {
            _switchManager.Clear();
            _profileManager.Load(filename);
            var lastProfiles = new LastProfiles {File = filename, Name = Path.GetFileNameWithoutExtension(filename)};
            if (_lastProfiles.Contains(lastProfiles))
            {
                _lastProfiles.Remove(lastProfiles);
                _lastProfiles.Insert(0, lastProfiles);
            }
            else
            {
                _lastProfiles.Insert(0, lastProfiles);
            }
            while (_lastProfiles.Count > 10)
            {
                _lastProfiles.RemoveAt(9);
            }
            SaveLastProfiles();
            OnPropertyChanged("ProfileManager");
        }
    
        private void SaveLastProfiles()
        {
            Settings.Default.RecentFiles.Clear();
            foreach (var profile in _lastProfiles)
            {
                Settings.Default.RecentFiles.Add(profile.File);
            }
            Settings.Default.Save();
        }
    }

    public class StringFormatConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                return values[1];
            }
            int number = ((int?)values[0]).Value;
            string format = values[1].ToString();
            return string.Format(format, number);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class LastProfiles
    {
        public string File { get; set; }
        public string Name { get; set; }

        protected bool Equals(LastProfiles other)
        {
            return string.Equals(File, other.File);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((LastProfiles) obj);
        }

        public override int GetHashCode()
        {
            return (File != null ? File.GetHashCode() : 0);
        }

        public static bool operator ==(LastProfiles left, LastProfiles right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LastProfiles left, LastProfiles right)
        {
            return !Equals(left, right);
        }
    }
}
