/**
 * \file
 *
 * \brief User Interface
 *
 * Copyright (c) 2014-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <asf.h>
#include "ioport.h"
#include "ui.h"
#include <string.h>

#define M_PA13 IOPORT_CREATE_PIN(PIOA, 13)
#define M_PA24 IOPORT_CREATE_PIN(PIOA, 24)
#define M_PA25 IOPORT_CREATE_PIN(PIOA, 25)
#define M_PB2 IOPORT_CREATE_PIN(PIOB, 2)
#define M_PB3 IOPORT_CREATE_PIN(PIOB, 3)
#define M_PD25 IOPORT_CREATE_PIN(PIOD, 25)

static bool send_idle = false;
static bool ui_b_led_blink = true;
static uint8_t ui_hid_report[UDI_HID_REPORT_IN_SIZE];
static uint8_t matrix_out[2] = {M_PA13, M_PD25};
static uint8_t matrix_in[2] = {M_PA24, M_PA25};
static uint8_t afec1_in[2] = {M_PB2, M_PB3};
static uint8_t button_states[((sizeof(matrix_out) * sizeof(matrix_in)) / 8) + 1];
//static uint8_t potentiometer_states[((sizeof(matrix_out) * sizeof(matrix_in)) / 8) + 1 + sizeof(afec1_in) + 2/* TODO 2 extra for debugginh */];

static void init_afec(void);
static void init_afec(void) {
	afec_enable(AFEC1);
	struct afec_config cfg;
	afec_get_config_defaults(&cfg);
	cfg.resolution = AFEC_12_BITS;
	cfg.mck = sysclk_get_cpu_hz();
	cfg.afec_clock = 16000000UL;
	cfg.startup_time = 8;
	cfg.settling_time = 0;
	cfg.tracktim = 0;
	cfg.transfer = 1;
	cfg.anach = 0;
	cfg.useq = 0;
	cfg.tag = 0;
	cfg.stm = 0;
	cfg.ibctl = 1;
	afec_init(AFEC1, &cfg);
	struct afec_ch_config ch_cfg;
	afec_ch_get_config_defaults(&ch_cfg);
	ch_cfg.diff = false;
	ch_cfg.gain = 1;
	afec_set_trigger(AFEC1, AFEC_TRIG_SW);
	for(uint8_t i=0; i<sizeof(afec1_in); i++) {
		ioport_set_pin_mode(afec1_in[i], 0);
		ioport_set_pin_dir(afec1_in[i], IOPORT_DIR_INPUT);
		afec_ch_set_config(AFEC1, i, &ch_cfg);
		afec_channel_enable(AFEC1, i);
		afec_channel_set_analog_offset(AFEC1, i, 0x800);
	}
	while (afec_start_calibration(AFEC1) != STATUS_OK);
	while (!(afec_get_interrupt_status(AFEC1) & AFEC_ISR_EOCAL));
	afec_set_writeprotect(AFEC1, true);
	afec_start_software_conversion(AFEC1);
}

void ui_init(void)
{
	/* Initialize LED */
	LED_Off(LED0);
	
	// Set multi-drive for matrix out
	for (uint8_t i=0; i<sizeof(matrix_out); i++) {
		ioport_set_pin_dir(matrix_out[i], IOPORT_DIR_OUTPUT);
		ioport_set_pin_mode(matrix_out[i], IOPORT_MODE_OPEN_DRAIN);
		ioport_set_pin_level(matrix_out[i], true);
	}
	// Set in for matrix in
	for (uint8_t i=0; i<sizeof(matrix_in); i++) {
		ioport_set_pin_mode(matrix_in[i], IOPORT_MODE_PULLUP);
		ioport_set_pin_dir(matrix_in[i], IOPORT_DIR_INPUT);
	}
	init_afec();
	memset(button_states, 0, sizeof(button_states));
}

void ui_powerdown(void)
{
	LED_Off(LED0);
}


void ui_wakeup_enable(void)
{
}

void ui_wakeup_disable(void)
{
}

void ui_wakeup(void)
{
	LED_On(LED0);
}

static void scan_potentiometers(uint8_t*);
static void scan_potentiometers(uint8_t *current_state) {
	// TODO: Remove... Set button states for debugging
	/*if ((afec_get_interrupt_status(AFEC1) & (1 << AFEC_CHANNEL_0))) {
		uint32_t afec1_ch0 = afec_channel_get_value(AFEC1, AFEC_CHANNEL_0);
		current_state[0] = (afec1_ch0 >> 4) & 0xff;
		current_state[3] = (afec1_ch0 >> 8) & 0x0f;
		current_state[4] = (afec1_ch0) & 0xff;
	}*/

	bool start_conversion_afec1 = false;
	for (uint8_t afec1_ch = 0/* TODO: Should be 0 normally instead of 1 */; afec1_ch<sizeof(afec1_in); afec1_ch++) {
		if ((afec_get_interrupt_status(AFEC1) & (1 << afec1_ch))) {
			uint32_t val = afec_channel_get_value(AFEC1, afec1_ch);
			start_conversion_afec1 = true;
			current_state[afec1_ch] = (val >> 4) & 0xff;
			//current_state[afec1_ch] = 0x00;
		}
	}
	if (start_conversion_afec1) afec_start_software_conversion(AFEC1);
}

void hid_get_report_request(udd_ctrl_request_t* ctrlreq);

void hid_get_report_request(udd_ctrl_request_t* ctrlreq) {
	uint8_t bmRequestType = ctrlreq->req.bmRequestType;
	uint8_t bRequest = ctrlreq->req.bRequest;
	le16_t wValue = ctrlreq->req.wValue;
	if (bmRequestType == 0xA1 && bRequest == 0x01) { // GET_REPORT
		if (wValue == 0x0101) { // Request IN report ID 01
			ui_hid_report[0]=0x01;
			memcpy(&ui_hid_report[1], button_states, sizeof(button_states));
			udd_set_setup_payload(ui_hid_report, sizeof(ui_hid_report));
		}
		if (wValue == 0x0102) { // Request IN report ID 02
			ui_hid_report[0]=0x02;
			memcpy(&ui_hid_report[1], button_states, sizeof(button_states));
			udd_set_setup_payload(ui_hid_report, sizeof(ui_hid_report));
		}
	}
}

static void scan_keys(void);
static void scan_keys(void) {
	static uint8_t bit = 0;
	static uint8_t current_state[sizeof(button_states)];
	static uint8_t scanrow = 0;
	static bool resend = false;
	if (resend) {
		ui_hid_report[0]=0x01;  // Report ID 1
		memcpy(&ui_hid_report[1], button_states, sizeof(button_states));
		if (!udi_hid_generic_send_report_in(ui_hid_report, sizeof(ui_hid_report))) {
			resend = false;
		}
	}
	//scan_potentiometers(current_state);
	for (uint8_t j=0; j<sizeof(matrix_in); j++) {
		// TODO: Currently supports 8 buttons only
		current_state[0] |= ioport_get_pin_level(matrix_in[j]) << bit++;
	}
	if (scanrow == sizeof(matrix_out) - 1 && (memcmp(button_states, current_state, sizeof(button_states)) || send_idle)) {
		send_idle = false;
		memcpy(button_states, current_state, sizeof(current_state));
		ui_hid_report[0]=0x01;  // Report ID 1
		memcpy(&ui_hid_report[1], button_states, sizeof(button_states));
		bool a = udi_hid_generic_send_report_in(ui_hid_report, sizeof(ui_hid_report));
		if (!a) {
			resend = true;
		}
	}
	ioport_set_pin_level(matrix_out[scanrow], true);
	if (++scanrow == sizeof(matrix_out)) {
		scanrow = 0;
		bit = 0;
		memset(current_state, 0, sizeof(current_state));
	}
	ioport_set_pin_level(matrix_out[scanrow], false);
}

void ui_process(uint16_t framenumber)
{
	scan_keys();
	
	bool b_btn_state;
	static bool btn0_last_state = false;
	static uint8_t cpt_sof = 0;

	// Blink LED
	if(ui_b_led_blink) {
		if ((framenumber % 1000) == 0) {
			LED_On(LED0);
		}
		if ((framenumber % 1000) == 500) {
			LED_Off(LED0);
			send_idle = true;
		}
	}
	/* Scan process running each 40ms */
	cpt_sof++;
	if (cpt_sof < 40) {
		return;
	}
	cpt_sof = 0;

	/* Scan push buttons 1 and 2 */
	b_btn_state = !ioport_get_pin_level(GPIO_PUSH_BUTTON_1);
	if (b_btn_state != btn0_last_state) {
		ui_hid_report[0]=0x01;  // Report ID 1
		ui_hid_report[1]=b_btn_state;
		//udi_hid_generic_send_report_in(ui_hid_report);
		btn0_last_state = b_btn_state;
	}
}

void ui_led_change(uint8_t *report)
{
	if (report[0]=='1') {
		/* A LED must be on */
		switch(report[1]) {
			case '1':
			ui_b_led_blink = false;
			LED_On(LED0);
			break;
		}
	} else {
		/* A LED can blink */
		switch(report[1]) {
			case '1':
			ui_b_led_blink = true;
			break;
		}
	}
}

/**
 * \defgroup UI User Interface
 *
 * Human interface on SAM4E-XPRO:
 * - LED0 blinks when USB host has checked and enabled HID generic interface
 * - HID events LED1 can make LED0 always on or blinking
 * - Event buttons are linked to SW0.
 *
 */
